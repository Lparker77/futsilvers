<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="totw1div">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltotws.php"><div id="alltotwbutton">ALL TOTWS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">Team of the week 1 silvers</div>
                </div>
                <div id="righttotwheader">
                    <a href="totw2.php"><div id="nexttotwbutton">NEXT TOTW</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="playercardinfosimilar">
            
            <div class="totwplayersrow1">
                <a href="index.php?id=71">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $mahrezrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $mahrezrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $mahrezrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $mahrezrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $mahrezrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $mahrezrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $mahrezrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $mahrezrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $mahrezrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $mahrezrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $mahrezrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $mahrezrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $mahrezrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $mahrezrow[19] ?></div>
                            <div class="totwplayername"><?php echo $mahrezrow[0] . '&nbsp' .  $mahrezrow[1].',&nbsp'.$mahrezrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Riyad Mahrez scored a brace against Sunderland in an impressive start to the season for Leicester City. <br><br>

                           His inform card makes an already great looking silver card even better with a boost to his dribbling, shooting and passing stats. He looks like the ideal right midfielder or winger for a Barclays Premier League silver team and I'll definitely be using him in my team. <br><br>

                           I don't expect him to cost much more than 30k during the week with the chance of rising in price in the future. A definite pick up for any silver user.
                        </div>
                    </div>
                
                   
            </div>
           

           <div class="totwplayersrow2">
                <a href="index.php?id=72">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $sarow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $sarow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $sarow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $sarow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $sarow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $sarow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $sarow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $sarow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $sarow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $sarow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $sarow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $sarow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $sarow[26] ?>
                        </div>
                    </div>
                </a>
                    
                   <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $sarow[19] ?></div>
                            <div class="totwplayername"><?php echo $sarow[0] . '&nbsp' .  $sarow[1].',&nbsp'.$sarow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Reading's latest signing Orlando Sa scored a hat-trick on the opening day of the season and for this reason makes it into Team of the Week 1. <br><br>

                           This inform card doesn't look like the best Championship striker but definitely worth trying out as a target man. I wouldn't use him over the likes of Abel Hernandez or Ross McCormack but again, he's another unique option. <br><br>

                           I don't expect him to cost much more than discard price, around 10k. I also don't expect his price will rise in the future so only pick him up if you plan on using him.
                        </div>
                    </div>
                
                   
            </div>
               

               <div class="totwplayersrow1">
                <a href="index.php?id=73">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $uriberow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $uriberow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $uriberow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $uriberow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $uriberow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $uriberow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $uriberow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $uriberow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $uriberow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $uriberow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $uriberow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $uriberow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $uriberow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $uriberow[19] ?></div>
                            <div class="totwplayername"><?php echo $uriberow[0] . '&nbsp' .  $uriberow[1].',&nbsp'.$uriberow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           I don't know much about the Colombian striker Uribe but what I do know is he scored 4 goals in Toluca's 4-0 win at the weekend. <br><br>

                           Although this inform card doesn't look anywhere near as good as his Team of the Season card from last year, it could work nicely up top in a Liga MX squad. <br><br>

                           Like Sa, I don't expect him to cost much more than discard price, around 10k. Again, I don't expect his price will rise in the future but you never know, he could become quite rare.
                        </div>
                    </div>
                
                   
            </div>


            <div class="totwplayersrow1">
                <a href="index.php?id=70">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $farkasrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $farkasrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $farkasrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $farkasrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $farkasrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $farkasrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $farkasrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $farkasrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $farkasrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $farkasrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $farkasrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $farkasrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $farkasrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                   <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $farkasrow[19] ?></div>
                            <div class="totwplayername"><?php echo $farkasrow[0] . '&nbsp' .  $farkasrow[1].',&nbsp'.$farkasrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Another player I don't know much about is Patrick Farkas the left back for SV Mattersburg who got 1 goal and 2 assists last week. <br><br>

                           Although his inform card doesn't look amazing, with limited options in the left back position for Austrian Bundesliga or Austrian silver squads he looks like a nice addition with his well rounded stats and defensive workrates. <br><br>

                           I expect him to cost 8-15k depending on his rarity but maybe even less because of how niche the Austrian Bundesliga is.
                        </div>
                    </div>
                
                   
            </div>

        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>