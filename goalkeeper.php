<!--http://php.net/manual/en/function.mysql-fetch-row.php-->

<?php

$playerID = $_GET["id"];

$result = mysql_query("SELECT /*row[0]*/firstname,/*row[1]*/surname,/*row[2]*/playerimage,/*row[3]*/club,/*row[4]*/clubimage,/*row[5]*/league,/*row[6]*/leagueimage,/*row[7]*/nationality,/*row[8]*/nationalityimage,/*row[9]*/age,
/*row[10]*/height,/*row[11]*/foot,/*row[12]*/attackworkrate,/*row[13]*/defensiveworkrate,/*row[14]*/skillmoves,/*row[15]*/weakfoot,/*row[16]*/traits,/*row[17]*/cardtype,/*row[18]*/cardimage,/*row[19]*/overallrating,/*row[20]*/position,
/*row[21]*/pacdiv,/*row[22]*/shohan,/*row[23]*/paskic,/*row[24]*/driref,/*row[25]*/defspe,/*row[26]*/phypos,/*row[27]*/totalcardstats,/*row[28]*/acceleration,/*row[29]*/sprintspeed,/*row[30]*/agility,/*row[31]*/balance,/*row[32]*/reactions,
/*row[33]*/ballcontrol,/*row[34]*/dribbling,/*row[35]*/attpositioning,/*row[36]*/finishing,/*row[37]*/shotpower,/*row[38]*/longshots,/*row[39]*/volleys,/*row[40]*/penalties,/*row[41]*/interceptions,/*row[42]*/headingacc,/*row[43]*/marking,
/*row[44]*/standingtackle,/*row[45]*/slidingtackle,/*row[46]*/vision,/*row[47]*/crossing,/*row[48]*/freekickacc,/*row[49]*/shortpassing,/*row[50]*/longpassing,/*row[51]*/curve,/*row[52]*/jumping,/*row[53]*/stamina,/*row[54]*/strength,
/*row[55]*/aggression,/*row[56]*/totalingamestats, /*row[57]*/ playervote FROM players WHERE id = '$playerID'");

if (!$result) {
	echo 'Could not run query: ' . mysql_error();
	exit;
	}
$row = mysql_fetch_row($result);
?>


<div id="playerinfo">

        	<div id="playerheader">
        		<div id="leftplayerheader">
        			<a href="#"><div id="reviewsbutton">REVIEWS</div></a>
        			<a href="#"><div id="similarbutton">SIMILAR</div></a>
        		</div>
            	<div id="centerplayerheader">
            		<div id="playername">
            			<?php echo $row[0] . '&nbsp' . $row[1] ?>
            		</div>
            		<div class="cardtype">
            			<?php echo $row[17] ?>
            		</div>
            	</div>
            	<div id="rightplayerheader">
            		<div id="thumbsdown"><img id="thumbs" src="img/thumbsdown.png"></div>
            		<div id="total">
                        <?php echo $row[57] ?>
                    </div>
            		<div id="thumbsup"><img id="thumbs" src="img/thumbsup.png"></div>
            	</div>
		</div>

		<div id="playercardinfo">
            <div class="playerinfoleft">
                <div class="overalldiv">
                    <div class="righttextrightdiv">
                    	<?php echo $row[11] ?>
                    </div>
                    <div class="headersrightdiv">FOOT</div>
                </div>
                <div class="clubdiv">
                    <div class="righttextrightdiv">
                    	<?php echo $row[12] ?>
                    </div>
                    <div class="headersrightdiv">ATTACK WORKRATE</div>
                </div>
                <div class="leaguediv">
                    <div class="righttextrightdiv">
                    	<?php echo $row[13] ?>
                    </div>
                    <div class="headersrightdiv">DEFENSIVE WORKRATE</div>
                </div>
                <div class="nationdiv">
                    <div class="skillmovesvaluediv">
                    	<?php echo $row[14] ?>
                    </div>
                    <div id="skillmoves1">★</div>
                    <div id="skillmoves2">★★</div>
                    <div id="skillmoves3">★★★</div>
                    <div id="skillmoves4">★★★★</div>
                    <div id="skillmoves5">★★★★★</div>
                    <div id="skillmoveslabeldiv">SKILL MOVES</div>
                </div>
                <div class="agediv">
                    <div class="weakfootvaluediv">
                    	<?php echo $row[15] ?>
                    </div>
                    <div id="weakfoot1">★</div>
                    <div id="weakfoot2">★★</div>
                    <div id="weakfoot3">★★★</div>
                    <div id="weakfoot4">★★★★</div>
                    <div id="weakfoot5">★★★★★</div>
                    <div id="weakfootlabeldiv">WEAK FOOT</div>
                </div>
                <div class="heightdiv">
                    <div class="traitsrightdiv">
                    	<?php echo $row[16] ?>
                    </div>
                    <div class="traitslabeldiv">TRAITS</div>
                </div>
            </div>

            <div class="playerinforight">
                <div class="overalldiv">
                    <div class="overallright">
                    	<?php echo $row[19] ?>
                    </div>
                    <div class="overall">OVERALL</div>
                </div>
                <div class="clubdiv">
                    <div class="clubrightimg"><img id="rightimg" <?php echo $row[4] ?> </div>
                    <div class="righttext">
                    	<?php echo $row[3] ?>
                    </div>
                    <div class="club">CLUB</div>
                </div>
                <div class="leaguediv">
                    <div class="clubrightimg"><img id="rightimg" <?php echo $row[6] ?> </div>
                    <div class="righttext">
                    	<?php echo $row[5] ?>
                    </div>
                    <div class="league">LEAGUE</div>
                </div>
                <div class="nationdiv">
                    <div class="clubrightimg"><img id="rightimg" <?php echo $row[8] ?></div>
                    <div class="righttext">
                    	<?php echo $row[7] ?>
                    </div>
                    <div class="nation">NATION</div>
                </div>
                <div class="agediv">
                    <div class="righttext">
                    	<?php echo $row[9] ?>
                    </div>
                    <div class="age">AGE</div>
                </div>
                <div class="heightdiv">
                    <div class="righttext">
                    	<?php echo $row[10] ?>
                    </div>
                    <div class="height">HEIGHT</div>
                </div>
            </div>
			<div id="playercard">
                <div id="playercardarea">
                    <img id="card" <?php echo $row[18] ?>
                    <div id="playerimgdiv"><img id="playerimg" <?php echo $row[2] ?></div>
                    <div id="playerrating">
                    	<?php echo $row[19] ?>
                    </div>
                    <div class="position">
                    	<?php echo $row[20] ?>
                    </div>
                    <div class="playerclub"><img id="badge" <?php echo $row[4] ?></div>
                    <div class="playerflag"><img id="flag" <?php echo $row[8] ?></div>
                    <div id="cardname">
                    	<?php echo $row[1] ?>
                    </div>
                    <div id="pacetag">DIV</div>
                    <div id="dribblingtag">REF</div>
                    <div id="shootingtag">HAN</div>
                    <div id="defendingtag">SPE</div>
                    <div id="passingtag">KIC</div>
                    <div id="physicaltag">POS</div>
                    <div id="pace">
                    	<?php echo $row[21] ?>
                    </div>
                    <div id="dribbling">
                    	<?php echo $row[24] ?>
                    </div>
                    <div id="shooting">
                    	<?php echo $row[22] ?>
                    </div>
                    <div id="defending">
                    	<?php echo $row[25] ?>
                    </div>
                    <div id="passing">
                    	<?php echo $row[23] ?>
                    </div>
                    <div id="physical">
                    	<?php echo $row[26] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


      