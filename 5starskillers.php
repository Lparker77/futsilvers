<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="all5starskillersdiv">
    <div class="allsilversspacer">

            <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">All 5 star skillers</div>
                </div>
                <div id="righttotwheader">
                    
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="playercardinfosimilar">
            
            <div class="informplayersrow1">
                <a href="index.php?id=59">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $silvarow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $silvarow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $silvarow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $silvarow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $silvarow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $silvarow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $silvarow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $silvarow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $silvarow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $silvarow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $silvarow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $silvarow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $silvarow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=60">
                    <div class="rowcard2">
                        <img id="similarcard" <?php echo $eltonrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $eltonrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $eltonrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $eltonrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $eltonrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $eltonrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $eltonrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $eltonrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $eltonrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $eltonrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $eltonrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $eltonrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $eltonrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=61">
                    <div class="rowcard3">
                        <img id="similarcard" <?php echo $kelvinrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $kelvinrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $kelvinrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $kelvinrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $kelvinrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $kelvinrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $kelvinrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $kelvinrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $kelvinrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $kelvinrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $kelvinrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $kelvinrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $kelvinrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=62">
                    <div class="rowcard4">
                        <img id="similarcard" <?php echo $aldossarirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $aldossarirow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $aldossarirow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $aldossarirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $aldossarirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $aldossarirow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $aldossarirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $aldossarirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $aldossarirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $aldossarirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $aldossarirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $aldossarirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $aldossarirow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=63">
                    <div class="rowcard5">
                        <img id="similarcard" <?php echo $arroyorow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $arroyorow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $arroyorow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $arroyorow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $arroyorow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $arroyorow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $arroyorow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $arroyorow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $arroyorow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $arroyorow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $arroyorow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $arroyorow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $arroyorow[26] ?>
                        </div>
                    </div>
                </a>
            </div>
            <div class="informplayersrow2">
                    <a href="index.php?id=64">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $elyounoussirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $elyounoussirow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $elyounoussirow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $elyounoussirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $elyounoussirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $elyounoussirow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $elyounoussirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $elyounoussirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $elyounoussirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $elyounoussirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $elyounoussirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $elyounoussirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $elyounoussirow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=65">
                    <div class="rowcard2">
                        <img id="similarcard" <?php echo $martinezrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $martinezrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $martinezrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $martinezrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $martinezrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $martinezrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $martinezrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $martinezrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $martinezrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $martinezrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $martinezrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $martinezrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $martinezrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=66">
                    <div class="rowcard3">
                        <img id="similarcard" <?php echo $mackaystevenrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $mackaystevenrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $mackaystevenrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $mackaystevenrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $mackaystevenrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $mackaystevenrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $mackaystevenrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $mackaystevenrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $mackaystevenrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $mackaystevenrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $mackaystevenrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $mackaystevenrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $mackaystevenrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=67">
                    <div class="rowcard4">
                        <img id="similarcard" <?php echo $ciftirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $ciftirow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $ciftirow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $ciftirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $ciftirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $ciftirow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $ciftirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $ciftirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $ciftirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $ciftirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $ciftirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $ciftirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $ciftirow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=68">
                    <div class="rowcard5">
                        <img id="similarcard" <?php echo $mastourrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $mastourrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $mastourrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $mastourrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $mastourrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $mastourrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $mastourrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $mastourrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $mastourrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $mastourrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $mastourrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $mastourrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $mastourrow[26] ?>
                        </div>
                    </div>
                </a>
            </div>
            <div class="informplayersrow3">
                   <a href="index.php?id=69">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $botakarow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $botakarow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $botakarow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $botakarow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $botakarow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $botakarow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $botakarow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $botakarow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $botakarow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $botakarow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $botakarow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $botakarow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $botakarow[26] ?>
                        </div>
                    </div>
                </a>
        </div>

               

        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>