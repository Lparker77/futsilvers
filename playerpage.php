<!doctype html>
<html class="no-js" lang="">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="favicon" type="img/ico" href="favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
       
    </head>

<body>

        <?php include 'header.php';?>
        
        <div id="searchbox">
          SEARCH FOR SILVER PLAYERS TO VIEW STATS AND REVIEWS
          <hr>
            <form>
                <input type="text" name="firstname" id="search" placeholder="SEARCH FOR SILVER PLAYERS"><br>
            </form>
        </div>	

        <div id="playerinfo">

        	<div id="playerheader">
        		<div id="leftplayerheader">
        			<a href="reviews.php"><div id="reviewsbutton">REVIEWS</div></a>
        			<a href="similar.php"><div id="similarbutton">SIMILAR</div></a>
        		</div>
            	<div id="centerplayerheader">
            		<div id="playername">PLAYER NAME</div>
            		<div id="cardtype">CARD TYPE</div>
            	</div>
            	<div id="rightplayerheader">
            		<div id="thumbsdown"><img id="thumbs" src="img/thumbsdown.png"></div>
            		<div id="total">0</div>
            		<div id="thumbsup"><img id="thumbs" src="img/thumbsup.png"></div>
            	</div>
		</div>

		<div id="playercardinfo">
            <div class="playerinfoleft">
                <div class="overalldiv">
                    <div class="righttextrightdiv">Left</div>
                    <div class="headersrightdiv">FOOT</div>
                </div>
                <div class="clubdiv">
                    <div class="righttextrightdiv">Medium</div>
                    <div class="headersrightdiv">ATTACK WORKRATE</div>
                </div>
                <div class="leaguediv">
                    <div class="righttextrightdiv">Low</div>
                    <div class="headersrightdiv">DEFENSIVE WORKRATE</div>
                </div>
                <div class="nationdiv">
                    <div class="righttextrightdiv">★★★★</div>
                    <div class="headersrightdiv">SKILL MOVES</div>
                </div>
                <div class="agediv">
                    <div class="righttextrightdiv">★★★</div>
                    <div class="headersrightdiv">WEAK FOOT</div>
                </div>
                <div class="heightdiv">
                    <div class="righttextrightdiv">None</div>
                    <div class="headersrightdiv">TRAITS</div>
                </div>
            </div>

            <div class="playerinforight">
                <div class="overalldiv">
                    <div class="overallright">74</div>
                    <div class="overall">OVERALL</div>
                </div>
                <div class="clubdiv">
                    <div class="clubrightimg"><img id="rightimg" src="img/badge/Chelsea.png"></div>
                    <div class="righttext">Chelsea</div>
                    <div class="club">CLUB</div>
                </div>
                <div class="leaguediv">
                    <div class="clubrightimg"><img id="rightimg" src="img/league/BPL.png"></div>
                    <div class="righttext">Barclays Premier League</div>
                    <div class="league">LEAGUE</div>
                </div>
                <div class="nationdiv">
                    <div class="clubrightimg"><img id="rightimg" src="img/flag/BurkinaFaso.png"></div>
                    <div class="righttext">Burkina Faso</div>
                    <div class="nation">NATION</div>
                </div>
                <div class="agediv">
                    <div class="righttext">19</div>
                    <div class="age">AGE</div>
                </div>
                <div class="heightdiv">
                    <div class="righttext">180cm / 5'10"</div>
                    <div class="height">HEIGHT</div>
                </div>
            </div>
			<div id="playercard">
                <div id="playercardarea">
                    <img id="card" src="img/cards/Silver-Rare.png">
                    <div id="playerimgdiv"><img id="playerimg" src="img/photo/Traore.png"></div>
                    <div id="playerrating">74</div>
                    <div class="position">ST</div>
                    <div class="playerclub"><img id="badge" src="img/badge/Chelsea.png"></div>
                    <div class="playerflag"><img id="flag" src="img/flag/BurkinaFaso.png"></div>
                    <div class="cardname">TRAORÉ</div>
                    <div class="pacetag">PAC</div>
                    <div class="dribblingtag">DRI</div>
                    <div class="shootingtag">SHO</div>
                    <div class="defendingtag">DEF</div>
                    <div class="passingtag">PAS</div>
                    <div class="physicaltag">PHY</div>
                    <div class="pace">85</div>
                    <div class="dribbling">78</div>
                    <div class="shooting">75</div>
                    <div class="defending">32</div>
                    <div class="passing">72</div>
                    <div class="physical">71</div>
                </div>
            </div>
        </div>
    </div>


        <div id="ingamestats">
            <div class="ingamesheader">
                <div id="leftplayerheader">
                    <div class="totalstatsheader">TOTAL CARD STATS</div>
                    <div class="totalstatsnumber">413</div>
                </div>
                <div id="centerplayerheader">
                    <div class="ingamestatsheader">INGAME STATS</div>
                </div>
                <div id="rightplayerheader">
                    <div class="totalstatsheader">TOTAL INGAME STATS</div>
                    <div class="totalstatsnumber">1877</div>
                </div>
            </div>
            <div class="ingamesdiv">
                <div class="statboxleft1">
                    <div id="paceheader">
                        <div class="stattext">PACE</div>
                        <div class="pacenumber">85</div>
                    </div>
                    <div id="accelerationdiv">
                        <div class="accelerationtext">Acceleration</div>
                        <div class="accelerationnumber">87</div>
                    </div>
                    <div id="sprintspeeddiv">
                        <div class="sprintspeedtext">Sprint Speed</div>
                        <div class="sprintspeednumber">83</div>
                    </div>
                </div>

                <div class="statboxleft2">
                    <div id="dribblingheader">
                        <div class="stattext">DRIBBLING</div>
                        <div class="dribblingnumber">78</div>
                    </div>
                    <div id="agilitydiv">
                        <div class="agilitytext">Agility</div>
                        <div class="agilitynumber">82</div>
                    </div>
                    <div id="balancediv">
                        <div class="balancetext">Balance</div>
                        <div class="balancenumber">74</div>
                    </div>
                     <div id="reactionsdiv">
                        <div class="reactionstext">Reactions</div>
                        <div class="reactionsnumber">75</div>
                    </div>
                    <div id="ballcontroldiv">
                        <div class="ballcontroltext">Ball Control</div>
                        <div class="ballcontrolnumber">80</div>
                    </div>
                    <div id="dribblingstatdiv">
                        <div class="dribblingstattext">Dribbling</div>
                        <div class="dribblingstatnumber">77</div>
                    </div>
                </div>

                <div class="statboxleft3">
                    <div id="shootingheader">
                        <div class="stattext">SHOOTING</div>
                        <div class="shootingnumber">75</div>
                    </div>
                    <div id="attpositioningdiv">
                        <div class="attpositioningtext">Att. Positioning</div>
                        <div class="attpositioningnumber">70</div>
                    </div>
                    <div id="finishingdiv">
                        <div class="finishingtext">Finishing</div>
                        <div class="finishingnumber">74</div>
                    </div>
                     <div id="shotpowerdiv">
                        <div class="shotpowertext">Shot Power</div>
                        <div class="shotpowernumber">78</div>
                    </div>
                    <div id="longshotsdiv">
                        <div class="longshotstext">Long Shots</div>
                        <div class="longshotsnumber">76</div>
                    </div>
                    <div id="volleysdiv">
                        <div class="volleystext">Volleys</div>
                        <div class="volleysnumber">76</div>
                    </div>
                    <div id="penaltiesdiv">
                        <div class="penaltiestext">Penalties</div>
                        <div class="penaltiesnumber">66</div>
                    </div>
                </div>

                <div class="statboxright3">
                    <div id="physicalheader">
                        <div class="stattext">PHYSICAL</div>
                        <div class="physicalnumber">71</div>
                    </div>
                    <div id="jumpingdiv">
                        <div class="jumpingtext">Jumping</div>
                        <div class="jumpingnumber">68</div>
                    </div>
                    <div id="staminadiv">
                        <div class="staminatext">Stamina</div>
                        <div class="staminanumber">73</div>
                    </div>
                     <div id="strengthdiv">
                        <div class="strengthtext">Strength</div>
                        <div class="strengthnumber">71</div>
                    </div>
                    <div id="aggressiondiv">
                        <div class="aggressiontext">Aggression</div>
                        <div class="aggressionnumber">69</div>
                    </div>
                </div>

                <div class="statboxright2">
                    <div id="passingheader">
                        <div class="stattext">PASSING</div>
                        <div class="passingnumber">72</div>
                    </div>
                    <div id="visiondiv">
                        <div class="visiontext">Vision</div>
                        <div class="visionnumber">74</div>
                    </div>
                    <div id="crossingdiv">
                        <div class="crossingtext">Crossing</div>
                        <div class="crossingnumber">69</div>
                    </div>
                     <div id="freekickaccdiv">
                        <div class="freekickacctext">Free Kick Acc.</div>
                        <div class="freekickaccnumber">65</div>
                    </div>
                    <div id="shortpassingdiv">
                        <div class="shortpassingtext">Short Passing</div>
                        <div class="shortpassingnumber">73</div>
                    </div>
                    <div id="longpassingdiv">
                        <div class="longpassingtext">Long Passing</div>
                        <div class="longpassingnumber">69</div>
                    </div>
                    <div id="curvediv">
                        <div class="curvetext">Curve</div>
                        <div class="curvenumber">75</div>
                    </div>
                </div>

                <div class="statboxright1">
                    <div id="defendingheader">
                        <div class="stattext">DEFENDING</div>
                        <div class="defendingnumber">32</div>
                    </div>
                    <div id="interceptionsdiv">
                        <div class="interceptionstext">Interceptions</div>
                        <div class="interceptionsnumber">39</div>
                    </div>
                    <div id="headingaccdiv">
                        <div class="headingacctext">Heading Acc.</div>
                        <div class="headingaccnumber">55</div>
                    </div>
                     <div id="markingdiv">
                        <div class="markingtext">Marking</div>
                        <div class="markingnumber">30</div>
                    </div>
                    <div id="standingtacklediv">
                        <div class="standingtackletext">Standing Tackle</div>
                        <div class="standingtacklenumber">26</div>
                    </div>
                    <div id="slidingtacklediv">
                        <div class="slidingtackletext">Sliding Tackle</div>
                        <div class="slidingtacklenumber">23</div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'footer.php';?>

</body>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

</html>