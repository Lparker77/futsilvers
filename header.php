<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="favicon" type="img/ico" href="favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
    </head>
    <body>
        <header>
            <div id="left" style="height: 115px;">
                <a href="https://twitter.com/FUTSILVERS" target="_blank"><img id="icon" src="img/twitter icon.png"></a>
                <a href="https://www.facebook.com/futsilvers/" target="_blank"><img id="icon" src="img/facebook icon.png"></a>
                <a href="https://youtube.com/lparker77" target="_blank"><img id="icon" src="img/youtube icon.png"></a>
            </div>
            <div id="center" style="height: 115px;"><img id="logo" src="img/futsilverslogo.png"></div>
            <div id="right" style="height: 115px;">
                    <div id="register">LOGIN</div>
                    <div id="login">REGISTER</div>
            </div>

     <?php include 'oopsmessage.php';?>       

    <?php 



    $randomID = rand(1,84); // randomly selects page ID from 1-69

    ?>


    <div id="holder">

        </header>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
       

       <ul id="nav">
            <li class="dropdown"><a class="dropbtn" href="index.php">HOME</a></li>
            <li class="dropdown"><a class="dropbtn" href="allsilverplayers.php">PLAYERS</a>
            <div class="dropdown-content">
                <a href="allsilverplayers.php">ALL SILVER PLAYERS</a>
                <a href="informsilvers.php">INFORM SILVERS</a>
                <div class="reviewsdropdown-content"><a href="#">PERFECT LINKS</a></div>
                <a href="5starskillers.php">5 STAR SKILLERS</a>
                <a href="index.php?id=<?php echo $randomID ?>">RANDOM SILVER</a>
            </div>
            </li>
            <li class="dropdown"><a class="reviewsdropbtn" href="#">REVIEWS</a>
            <div class="dropdown-content">
                <div class="reviewsdropdown-content"><a href="#">TOP REVIEWS</a></div>
                <div class="reviewsdropdown-content"><a href="#">POPULAR REVIEWS</a></div>
                <div class="reviewsdropdown-content"><a href="#">RECENT REVIEWS</a></div>
            </div>
            </li>
            <li class="dropdown"><a class="dropbtn" href="alltotws.php">TOTW SILVERS</a>
            <div class="dropdown-bar">
                <div id="totwsilversbar">
                    <img id="totwthumbnail1" src="img/TOTW/TOTW1.png">
                    <img id="totwthumbnail2" src="img/TOTW/TOTW2.png">
                    <img id="totwthumbnail3" src="img/TOTW/TOTW3.png">
                    <img id="totwthumbnail4" src="img/TOTW/TOTW4.png">
                </div>
                <div class="viewallbutton1">VIEW ALL</div>
            </div>
            </li>
            <li class="dropdown"><a class="dropbtn" href="alltips.php">TIPS & TRICKS</a>
            <div class="dropdown-bar">
                <div id="tipstricksbar">
                    <img id="tipsthumbnail1" src="img/TipsTricks/cbthumbnail.png">
                    <img id="tipsthumbnail2" src="img/TipsTricks/fullbackthumbnail.png">
                    <img id="tipsthumbnail3" src="img/TipsTricks/midfielderthumbnail.png">
                    <img id="tipsthumbnail4" src="img/TipsTricks/wingerthumbnail.png">
                </div>
                <div class="viewallbutton2">VIEW ALL</div>
            </div>
            </li>
        </ul>

    </div>

        <div id="spacer"></div>

        <div id="black_overlay" style="width: 100%;"> </div>
        <div class="logindiv">
            <div class="close"><img id="closeimg" src="img/close.png"></div>
            <div class="loginheader">LOGIN</div>
            <form><input type="text" name="firstname" id="username" placeholder="Username"></form><br>
            <form><input type="text" name="firstname" id="password" placeholder="Password"></form><br>
            <div class="passwordcheckbox">
                <input type="checkbox" class="checkbox">
                <div class="checkboxtext">Keep me logged in?</div>
            </div>
            <div class="forgotpassword">Forgot your password?</div><br><br>
            <div class="loginbutton">LOGIN</div>
        </div>
        
        <div id="black_overlay" style="width: 100%;"> </div>
        <div class="registerdiv">
            <div class="close"><img id="closeimg" src="img/close.png"></div>
            <div class="loginheader">REGISTER</div>
            <form><input type="text" name="firstname" id="email" placeholder="Email"></form><br>
            <form><input type="text" name="firstname" id="username" placeholder="Choose Username"></form><br>
            <form><input type="text" name="firstname" id="password" placeholder="Password"></form><br>
            <form><input type="text" name="firstname" id="confirmpassword" placeholder="Confirm Password"></form><br>
            <div class="registerbutton">REGISTER</div>
        </div>

        


        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
