

<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="whattolookforinfullbackdiv">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltips.php"><div id="alltotwbutton">ALL TIPS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">what to look for in a silver fullback</div>
                </div>
                <div id="righttotwheader">
                    <a href="whattolookforinamidfielder.php"><div id="nexttotwbutton">NEXT TIP</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="tipsbackground">
            
            <div class="totwplayersrow1">
                
                    
                    
                        <div class="tipsheaderimage">
                            <img id="cbheaderimg" src="img/fullbacksgraphic.png">
                        </div>
                        <div class="tipstext">
                           Fullbacks are a lot easier to pick because in my opinion they don’t affect your team as much as other positions. When making hybrids you can usually get away with a fairly average player for the link as long as you have good centre backs and know how to defend. Most people will usually go for pace to counter the likelihood that your opponent will have quick forwards but much like centre backs, positioning and defensive stats are far more important in Fifa 16.<br><br>

The graphic  shows examples of left backs/right backs that you could use in your teams. I wont go into as much detail as I did with the centre backs as what you’re looking for is fairly similar, the only differences being that height isn’t as important and instead stats like dribbling and passing can be useful. All three of the players are viable options for your teams, Tshimanga being the ideal all round left back, Mercado the more defensive right back and Navarro the pacey player you’d choose if you can’t fit a player like the other two into your team.<br><br>

So to recap, when making a silver team look for left backs and right backs like Tshimanga. If you’d prefer a more defensive option choose someone like Mercado with 70+ pace and solid defensive stats. Lastly, you can get away with using a player like Navarro but don’t expect to rely on them defensively and don't be surprised if they make the occasional error.
                        </div>
                    </div>
                
                   
            
           


        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>