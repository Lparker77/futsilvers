<!--http://php.net/manual/en/function.mysql-fetch-row.php-->

<?php

$playerID = $_GET["id"];

$result = mysql_query("SELECT /*row[0]*/firstname,/*row[1]*/surname,/*row[2]*/playerimage,/*row[3]*/club,/*row[4]*/clubimage,/*row[5]*/league,/*row[6]*/leagueimage,/*row[7]*/nationality,/*row[8]*/nationalityimage,/*row[9]*/age,
/*row[10]*/height,/*row[11]*/foot,/*row[12]*/attackworkrate,/*row[13]*/defensiveworkrate,/*row[14]*/skillmoves,/*row[15]*/weakfoot,/*row[16]*/traits,/*row[17]*/cardtype,/*row[18]*/cardimage,/*row[19]*/overallrating,/*row[20]*/position,
/*row[21]*/pacdiv,/*row[22]*/shohan,/*row[23]*/paskic,/*row[24]*/driref,/*row[25]*/defspe,/*row[26]*/phypos,/*row[27]*/totalcardstats,/*row[28]*/acceleration,/*row[29]*/sprintspeed,/*row[30]*/agility,/*row[31]*/balance,/*row[32]*/reactions,
/*row[33]*/ballcontrol,/*row[34]*/dribbling,/*row[35]*/attpositioning,/*row[36]*/finishing,/*row[37]*/shotpower,/*row[38]*/longshots,/*row[39]*/volleys,/*row[40]*/penalties,/*row[41]*/interceptions,/*row[42]*/headingacc,/*row[43]*/marking,
/*row[44]*/standingtackle,/*row[45]*/slidingtackle,/*row[46]*/vision,/*row[47]*/crossing,/*row[48]*/freekickacc,/*row[49]*/shortpassing,/*row[50]*/longpassing,/*row[51]*/curve,/*row[52]*/jumping,/*row[53]*/stamina,/*row[54]*/strength,
/*row[55]*/aggression,/*row[56]*/totalingamestats, /*row[57]*/ playervote FROM players WHERE id = '$playerID'");

if (!$result) {
	echo 'Could not run query: ' . mysql_error();
	exit;
	}
$row = mysql_fetch_row($result);

// echo('<pre>');
// print_r($row);
// echo('</pre>');

?>


<div id="playerinfo">

        	<div id="playerheader">
        		<div id="leftplayerheader">
        			<a href="#"><div id="reviewsbutton">REVIEWS</div></a>
        			<a href="#"><div id="similarbutton">SIMILAR</div></a>
        		</div>
            	<div id="centerplayerheader">
            		<div id="playername">
            			<?php echo $row[0] . '&nbsp' . $row[1] ?>
            		</div>
            		<div class="cardtype">
            			<?php echo $row[17] ?>
            		</div>
            	</div>
            	<div id="rightplayerheader">
            		<div id="thumbsdown"><img id="thumbs" src="img/thumbsdown.png"></div>
            		<div id="total">
                        <?php echo $row[57] ?>
                    </div>
            		<div id="thumbsup"><img id="thumbs" src="img/thumbsup.png"></div>
            	</div>
		</div>

		<div id="playercardinfo">
            <div class="playerinfoleft">
                <div class="overalldiv">
                    <div class="righttextrightdiv">
                    	<?php echo $row[11] ?>
                    </div>
                    <div class="headersrightdiv">FOOT</div>
                </div>
                <div class="clubdiv">
                    <div class="righttextrightdiv">
                    	<?php echo $row[12] ?>
                    </div>
                    <div class="headersrightdiv">ATTACK WORKRATE</div>
                </div>
                <div class="leaguediv">
                    <div class="righttextrightdiv">
                    	<?php echo $row[13] ?>
                    </div>
                    <div class="headersrightdiv">DEFENSIVE WORKRATE</div>
                </div>
                <div class="nationdiv">
                    <div class="skillmovesvaluediv">
                    	<?php echo $row[14] ?>
                    </div>
                    <div id="skillmoves1">★</div>
                    <div id="skillmoves2">★★</div>
                    <div id="skillmoves3">★★★</div>
                    <div id="skillmoves4">★★★★</div>
                    <div id="skillmoves5">★★★★★</div>
                    <div id="skillmoveslabeldiv">SKILL MOVES</div>
                </div>
                <div class="agediv">
                    <div class="weakfootvaluediv">
                    	<?php echo $row[15] ?>
                    </div>
                    <div id="weakfoot1">★</div>
                    <div id="weakfoot2">★★</div>
                    <div id="weakfoot3">★★★</div>
                    <div id="weakfoot4">★★★★</div>
                    <div id="weakfoot5">★★★★★</div>
                    <div id="weakfootlabeldiv">WEAK FOOT</div>
                </div>
                <div class="heightdiv">
                    <div class="traitsrightdiv">
                    	<?php echo $row[16] ?>
                    </div>
                    <div class="traitslabeldiv">TRAITS</div>
                </div>
            </div>

            <div class="playerinforight">
                <div class="overalldiv">
                    <div class="overallright">
                    	<?php echo $row[19] ?>
                    </div>
                    <div class="overall">OVERALL</div>
                </div>
                <div class="clubdiv">
                    <div class="clubrightimg"><img id="rightimg" <?php echo $row[4] ?> </div>
                    <div class="righttext">
                    	<?php echo $row[3] ?>
                    </div>
                    <div class="club">CLUB</div>
                </div>
                <div class="leaguediv">
                    <div class="clubrightimg"><img id="rightimg" <?php echo $row[6] ?> </div>
                    <div class="righttext">
                    	<?php echo $row[5] ?>
                    </div>
                    <div class="league">LEAGUE</div>
                </div>
                <div class="nationdiv">
                    <div class="clubrightimg"><img id="rightimg" <?php echo $row[8] ?></div>
                    <div class="righttext">
                    	<?php echo $row[7] ?>
                    </div>
                    <div class="nation">NATION</div>
                </div>
                <div class="agediv">
                    <div class="righttext">
                    	<?php echo $row[9] ?>
                    </div>
                    <div class="age">AGE</div>
                </div>
                <div class="heightdiv">
                    <div class="righttext">
                    	<?php echo $row[10] ?>
                    </div>
                    <div class="height">HEIGHT</div>
                </div>
            </div>
			<div id="playercard">
                <div id="playercardarea">
                    <img id="card" <?php echo $row[18] ?>
                    <div id="playerimgdiv"><img id="playerimg" <?php echo $row[2] ?></div>
                    <div id="playerrating">
                    	<?php echo $row[19] ?>
                    </div>
                    <div class="position">
                    	<?php echo $row[20] ?>
                    </div>
                    <div class="playerclub"><img id="badge" <?php echo $row[4] ?></div>
                    <div class="playerflag"><img id="flag" <?php echo $row[8] ?></div>
                    <div id="cardname">
                    	<?php echo $row[1] ?>
                    </div>
                    <div id="pacetag">PAC</div>
                    <div id="dribblingtag">DRI</div>
                    <div id="shootingtag">SHO</div>
                    <div id="defendingtag">DEF</div>
                    <div id="passingtag">PAS</div>
                    <div id="physicaltag">PHY</div>
                    <div id="pace">
                    	<?php echo $row[21] ?>
                    </div>
                    <div id="dribbling">
                    	<?php echo $row[24] ?>
                    </div>
                    <div id="shooting">
                    	<?php echo $row[22] ?>
                    </div>
                    <div id="defending">
                    	<?php echo $row[25] ?>
                    </div>
                    <div id="passing">
                    	<?php echo $row[23] ?>
                    </div>
                    <div id="physical">
                    	<?php echo $row[26] ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <div id="ingamestats">
            <div class="ingamesheader">
                <div id="leftplayerheader">
                    <div class="totalstatsheader">TOTAL CARD STATS</div>
                    <div class="totalstatsnumber">
                        <?php echo $row[27] ?>
                    </div>
                </div>
                <div id="centerplayerheader">
                    <div class="ingamestatsheader">INGAME STATS</div>
                </div>
                <div id="rightplayerheader">
                    <div class="totalstatsheader">TOTAL INGAME STATS</div>
                    <div class="totalstatsnumber">
                        <?php echo $row[56] ?>
                    </div>
                </div>
            </div>
            <div class="ingamesdiv">
                <div class="statboxleft1">
                    <div id="paceheader">
                        <div class="stattext">PACE</div>
                        <div class="pacenumber">
                            <?php echo $row[21] ?>
                        </div>
                    </div>
                    <div id="accelerationdiv">
                        <div class="accelerationtext">Acceleration</div>
                        <div class="accelerationnumber">
                            <?php echo $row[28] ?>
                        </div>
                    </div>
                    <div id="sprintspeeddiv">
                        <div class="sprintspeedtext">Sprint Speed</div>
                        <div class="sprintspeednumber">
                            <?php echo $row[29] ?>
                        </div>
                    </div>
                </div>

                <div class="statboxleft2">
                    <div id="dribblingheader">
                        <div class="stattext">DRIBBLING</div>
                        <div class="dribblingnumber">
                            <?php echo $row[24] ?>
                        </div>
                    </div>
                    <div id="agilitydiv">
                        <div class="agilitytext">Agility</div>
                        <div class="agilitynumber">
                            <?php echo $row[30] ?>
                        </div>
                    </div>
                    <div id="balancediv">
                        <div class="balancetext">Balance</div>
                        <div class="balancenumber">
                            <?php echo $row[31] ?>
                        </div>
                    </div>
                     <div id="reactionsdiv">
                        <div class="reactionstext">Reactions</div>
                        <div class="reactionsnumber">
                            <?php echo $row[32] ?>
                        </div>
                    </div>
                    <div id="ballcontroldiv">
                        <div class="ballcontroltext">Ball Control</div>
                        <div class="ballcontrolnumber">
                            <?php echo $row[33] ?>
                        </div>
                    </div>
                    <div id="dribblingstatdiv">
                        <div class="dribblingstattext">Dribbling</div>
                        <div class="dribblingstatnumber">
                            <?php echo $row[34] ?>
                        </div>
                    </div>
                </div>

                <div class="statboxleft3">
                    <div id="shootingheader">
                        <div class="stattext">SHOOTING</div>
                        <div class="shootingnumber">
                            <?php echo $row[22] ?>
                        </div>
                    </div>
                    <div id="attpositioningdiv">
                        <div class="attpositioningtext">Att. Positioning</div>
                        <div class="attpositioningnumber">
                            <?php echo $row[35] ?>
                        </div>
                    </div>
                    <div id="finishingdiv">
                        <div class="finishingtext">Finishing</div>
                        <div class="finishingnumber">
                            <?php echo $row[36] ?>
                        </div>
                    </div>
                     <div id="shotpowerdiv">
                        <div class="shotpowertext">Shot Power</div>
                        <div class="shotpowernumber">
                            <?php echo $row[37] ?>
                        </div>
                    </div>
                    <div id="longshotsdiv">
                        <div class="longshotstext">Long Shots</div>
                        <div class="longshotsnumber">
                            <?php echo $row[38] ?>
                        </div>
                    </div>
                    <div id="volleysdiv">
                        <div class="volleystext">Volleys</div>
                        <div class="volleysnumber">
                            <?php echo $row[39] ?>
                        </div>
                    </div>
                    <div id="penaltiesdiv">
                        <div class="penaltiestext">Penalties</div>
                        <div class="penaltiesnumber">
                            <?php echo $row[40] ?>
                        </div>
                    </div>
                </div>

                <div class="statboxright3">
                    <div id="physicalheader">
                        <div class="stattext">PHYSICAL</div>
                        <div class="physicalnumber">
                            <?php echo $row[26] ?>
                        </div>
                    </div>
                    <div id="jumpingdiv">
                        <div class="jumpingtext">Jumping</div>
                        <div class="jumpingnumber">
                            <?php echo $row[52] ?>
                        </div>
                    </div>
                    <div id="staminadiv">
                        <div class="staminatext">Stamina</div>
                        <div class="staminanumber">
                            <?php echo $row[53] ?>
                        </div>
                    </div>
                     <div id="strengthdiv">
                        <div class="strengthtext">Strength</div>
                        <div class="strengthnumber">
                            <?php echo $row[54] ?>
                        </div>
                    </div>
                    <div id="aggressiondiv">
                        <div class="aggressiontext">Aggression</div>
                        <div class="aggressionnumber">
                            <?php echo $row[55] ?>
                        </div>
                    </div>
                </div>

                <div class="statboxright2">
                    <div id="passingheader">
                        <div class="stattext">PASSING</div>
                        <div class="passingnumber">
                            <?php echo $row[23] ?>
                        </div>
                    </div>
                    <div id="visiondiv">
                        <div class="visiontext">Vision</div>
                        <div class="visionnumber">
                            <?php echo $row[46] ?>
                        </div>
                    </div>
                    <div id="crossingdiv">
                        <div class="crossingtext">Crossing</div>
                        <div class="crossingnumber">
                            <?php echo $row[47] ?>
                        </div>
                    </div>
                     <div id="freekickaccdiv">
                        <div class="freekickacctext">Free Kick Acc.</div>
                        <div class="freekickaccnumber">
                            <?php echo $row[48] ?>
                        </div>
                    </div>
                    <div id="shortpassingdiv">
                        <div class="shortpassingtext">Short Passing</div>
                        <div class="shortpassingnumber">
                            <?php echo $row[49] ?>
                        </div>
                    </div>
                    <div id="longpassingdiv">
                        <div class="longpassingtext">Long Passing</div>
                        <div class="longpassingnumber">
                            <?php echo $row[50] ?>
                        </div>
                    </div>
                    <div id="curvediv">
                        <div class="curvetext">Curve</div>
                        <div class="curvenumber">
                            <?php echo $row[51] ?>
                        </div>
                    </div>
                </div>

                <div class="statboxright1">
                    <div id="defendingheader">
                        <div class="stattext">DEFENDING</div>
                        <div class="defendingnumber">
                            <?php echo $row[25] ?>
                        </div>
                    </div>
                    <div id="interceptionsdiv">
                        <div class="interceptionstext">Interceptions</div>
                        <div class="interceptionsnumber">
                            <?php echo $row[41] ?>
                        </div>
                    </div>
                    <div id="headingaccdiv">
                        <div class="headingacctext">Heading Acc.</div>
                        <div class="headingaccnumber">
                            <?php echo $row[42] ?>
                        </div>
                    </div>
                     <div id="markingdiv">
                        <div class="markingtext">Marking</div>
                        <div class="markingnumber">
                            <?php echo $row[43] ?>
                        </div>
                    </div>
                    <div id="standingtacklediv">
                        <div class="standingtackletext">Standing Tackle</div>
                        <div class="standingtacklenumber">
                            <?php echo $row[44] ?>
                        </div>
                    </div>
                    <div id="slidingtacklediv">
                        <div class="slidingtackletext">Sliding Tackle</div>
                        <div class="slidingtacklenumber">
                            <?php echo $row[45] ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>