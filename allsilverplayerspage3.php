<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="allsilversdivpage3">
    <div class="allsilversspacer">

            <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="allsilverplayerspage2.php"><div id="alltotwbutton">PREVIOUS PAGE</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">All Silver Players</div>
                </div>
                <div id="righttotwheader">
                    
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="playercardinfosimilar">
            
            <div class="informplayersrow1">
                <a href="index.php?id=46">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $semedorow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $semedorow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $semedorow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $semedorow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $semedorow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $semedorow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $semedorow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $semedorow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $semedorow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $semedorow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $semedorow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $semedorow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $semedorow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=47">
                    <div class="rowcard2">
                        <img id="similarcard" <?php echo $smolovrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $smolovrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $smolovrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $smolovrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $smolovrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $smolovrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $smolovrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $smolovrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $smolovrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $smolovrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $smolovrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $smolovrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $smolovrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=48">
                    <div class="rowcard3">
                        <img id="similarcard" <?php echo $sparvrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $sparvrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $sparvrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $sparvrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $sparvrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $sparvrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $sparvrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $sparvrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $sparvrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $sparvrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $sparvrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $sparvrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $sparvrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=49">
                    <div class="rowcard4">
                        <img id="similarcard" <?php echo $susorow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $susorow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $susorow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $susorow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $susorow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $susorow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $susorow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $susorow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $susorow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $susorow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $susorow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $susorow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $susorow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=50">
                    <div class="rowcard5">
                        <img id="similarcard" <?php echo $wakasorow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $wakasorow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $wakasorow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $wakasorow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $wakasorow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $wakasorow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $wakasorow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $wakasorow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $wakasorow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $wakasorow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $wakasorow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $wakasorow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $wakasorow[26] ?>
                        </div>
                    </div>
                </a>
            </div>
            <div class="informplayersrow2">
                    <a href="index.php?id=51">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $williamsrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $williamsrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $williamsrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $williamsrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $williamsrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $williamsrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $williamsrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $williamsrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $williamsrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $williamsrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $williamsrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $williamsrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $williamsrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=52">
                    <div class="rowcard2">
                        <img id="similarcard" <?php echo $wastonrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $wastonrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $wastonrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $wastonrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $wastonrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $wastonrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $wastonrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $wastonrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $wastonrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $wastonrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $wastonrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $wastonrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $wastonrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=53">
                    <div class="rowcard3">
                        <img id="similarcard" <?php echo $weiglrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $weiglrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $weiglrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $weiglrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $weiglrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $weiglrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $weiglrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $weiglrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $weiglrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $weiglrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $weiglrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $weiglrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $weiglrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=54">
                    <div class="rowcard4">
                        <img id="similarcard" <?php echo $nemrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $nemrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $nemrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $nemrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $nemrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $nemrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $nemrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $nemrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $nemrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $nemrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $nemrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $nemrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $nemrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=55">
                    <div class="rowcard5">
                        <img id="similarcard" <?php echo $forestierirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $forestierirow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $forestierirow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $forestierirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $forestierirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $forestierirow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $forestierirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $forestierirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $forestierirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $forestierirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $forestierirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $forestierirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $forestierirow[26] ?>
                        </div>
                    </div>
                </a>
            </div>
            <div class="informplayersrow3">
                   <a href="index.php?id=56">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $forrenrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $forrenrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $forrenrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $forrenrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $forrenrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $forrenrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $forrenrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $forrenrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $forrenrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $forrenrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $forrenrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $forrenrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $forrenrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=57">
                    <div class="rowcard2">
                        <img id="similarcard" <?php echo $formicarow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $formicarow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $formicarow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $formicarow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $formicarow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $formicarow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $formicarow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $formicarow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $formicarow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $formicarow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $formicarow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $formicarow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $formicarow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=58">
                    <div class="rowcard3">
                        <img id="similarcard" <?php echo $fornarolirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $fornarolirow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $fornarolirow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $fornarolirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $fornarolirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $fornarolirow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $fornarolirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $fornarolirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $fornarolirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $fornarolirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $fornarolirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $fornarolirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $fornarolirow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=59">
                    <div class="rowcard4">
                        <img id="similarcard" <?php echo $silvarow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $silvarow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $silvarow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $silvarow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $silvarow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $silvarow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $silvarow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $silvarow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $silvarow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $silvarow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $silvarow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $silvarow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $silvarow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=60">
                    <div class="rowcard5">
                        <img id="similarcard" <?php echo $eltonrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $eltonrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $eltonrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $eltonrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $eltonrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $eltonrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $eltonrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $eltonrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $eltonrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $eltonrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $eltonrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $eltonrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $eltonrow[26] ?>
                        </div>
                    </div>
                </a>
        </div>

                <div class="informplayersrow4">
                   <a href="index.php?id=61">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $kelvinrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $kelvinrow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $kelvinrow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $kelvinrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $kelvinrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $kelvinrow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $kelvinrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $kelvinrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $kelvinrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $kelvinrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $kelvinrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $kelvinrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $kelvinrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=62">
                    <div class="rowcard2">
                        <img id="similarcard" <?php echo $aldossarirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $aldossarirow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $aldossarirow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $aldossarirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $aldossarirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $aldossarirow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $aldossarirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $aldossarirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $aldossarirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $aldossarirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $aldossarirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $aldossarirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $aldossarirow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=63">
                    <div class="rowcard3">
                        <img id="similarcard" <?php echo $arroyorow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $arroyorow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $arroyorow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $arroyorow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $arroyorow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $arroyorow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $arroyorow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $arroyorow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $arroyorow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $arroyorow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $arroyorow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $arroyorow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $arroyorow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=64">
                    <div class="rowcard4">
                        <img id="similarcard" <?php echo $elyounoussirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $elyounoussirow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $elyounoussirow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $elyounoussirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $elyounoussirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $elyounoussirow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $elyounoussirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $elyounoussirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $elyounoussirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $elyounoussirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $elyounoussirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $elyounoussirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $elyounoussirow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=65">
                    <div class="rowcard5">
                        <img id="similarcard" <?php echo $martinezrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $martinezrow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $martinezrow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $martinezrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $martinezrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $martinezrow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $martinezrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $martinezrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $martinezrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $martinezrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $martinezrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $martinezrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $martinezrow[26] ?>
                        </div>
                    </div>
                </a>
        </div>

        <div class="informplayersrow5">
                   <a href="index.php?id=66">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $mackaystevenrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $mackaystevenrow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $mackaystevenrow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $mackaystevenrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $mackaystevenrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $mackaystevenrow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $mackaystevenrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $mackaystevenrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $mackaystevenrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $mackaystevenrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $mackaystevenrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $mackaystevenrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $mackaystevenrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=67">
                    <div class="rowcard2">
                        <img id="similarcard" <?php echo $ciftirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $ciftirow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $ciftirow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $ciftirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $ciftirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $ciftirow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $ciftirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $ciftirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $ciftirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $ciftirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $ciftirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $ciftirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $ciftirow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=68">
                    <div class="rowcard3">
                        <img id="similarcard" <?php echo $mastourrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $mastourrow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $mastourrow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $mastourrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $mastourrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $mastourrow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $mastourrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $mastourrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $mastourrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $mastourrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $mastourrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $mastourrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $mastourrow[26] ?>
                        </div>
                    </div>
                </a>
                    <a href="index.php?id=69">
                    <div class="rowcard4">
                        <img id="similarcard" <?php echo $botakarow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $botakarow[2] ?></div>
                        <div id="reviewplayerrating">
                            <?php echo $botakarow[19] ?>
                        </div>
                        <div class="reviewposition">
                            <?php echo $botakarow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $botakarow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $botakarow[8] ?></div>
                        <div class="rowcardname">
                            <?php echo $botakarow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                            <?php echo $botakarow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                            <?php echo $botakarow[24] ?>
                        </div>
                        <div class="reviewshooting">
                            <?php echo $botakarow[22] ?>
                        </div>
                        <div class="reviewdefending">
                            <?php echo $botakarow[25] ?>
                        </div>
                        <div class="reviewpassing">
                            <?php echo $botakarow[23] ?>
                        </div>
                        <div class="reviewphysical">
                            <?php echo $botakarow[26] ?>
                        </div>
                    </div>
                </a>
                    
        </div>
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>

