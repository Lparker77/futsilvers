<!doctype html>
<html class="no-js" lang="">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="favicon" type="img/ico" href="favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
       
    </head>

<body>

        <?php include 'header.php';?>
        
        <div id="searchbox">
          SEARCH FOR SILVER PLAYERS TO VIEW STATS AND REVIEWS
          <hr>
            <form>
                <input type="text" name="firstname" id="search" placeholder="SEARCH FOR SILVER PLAYERS"><br>
            </form>
        </div>  

        <div class="similarplayersdiv">

            <div id="playerheader">
                <div id="leftplayerheader">
                    <a href="reviews.html"><div id="reviewsbutton">REVIEWS</div></a>
                    <a href="playerpage.html"><div id="similarbutton">PROFILE</div></a>
                </div>
                <div id="centerplayerheader">
                    <div id="playername">PLAYER NAME</div>
                    <div id="cardtype">SIMILAR PLAYERS</div>
                </div>
                <div id="rightplayerheader">
                    <div id="thumbsdown"><img id="thumbs" src="img/thumbsdown.png"></div>
                    <div id="total">0</div>
                    <div id="thumbsup"><img id="thumbs" src="img/thumbsup.png"></div>
                </div>
        </div>

        <div class="playercardinfosimilar">
            <div class="playercardsimilar">
                <div id="playercardarea">
                    <img id="card" src="img/cards/Silver-Rare.png">
                    <div id="playerimgdiv"><img id="playerimg" src="img/photo/Traore.png"></div>
                    <div id="playerrating">74</div>
                    <div class="position">ST</div>
                    <div class="playerclub"><img id="badge" src="img/badge/Chelsea.png"></div>
                    <div class="playerflag"><img id="flag" src="img/flag/BurkinaFaso.png"></div>
                    <div class="cardname">TRAORÉ</div>
                    <div class="pacetag">PAC</div>
                    <div class="dribblingtag">DRI</div>
                    <div class="shootingtag">SHO</div>
                    <div class="defendingtag">DEF</div>
                    <div class="passingtag">PAS</div>
                    <div class="physicaltag">PHY</div>
                    <div class="pace">85</div>
                    <div class="dribbling">78</div>
                    <div class="shooting">75</div>
                    <div class="defending">32</div>
                    <div class="passing">72</div>
                    <div class="physical">71</div>
                </div>
            </div>
            <div class="similarplayersrow1">
                    <div class="rowcard1">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard2">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard3">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard4">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
            </div>
            <div class="similarplayersrow2">
                    <div class="rowcard1">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard2">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard3">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard4">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
            </div>
            <div class="similarplayersrow3">
                    <div class="rowcard1">
                       <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard2">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard3">
                        <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
                    <div class="rowcard4">
                       <img id="similarcard" src="img/cards/Silver-Rare.png">
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                        <div id="reviewplayerrating">74</div>
                        <div class="reviewposition">ST</div>
                        <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                        <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                        <div class="rowcardname">TRAORÉ</div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">85</div>
                        <div class="reviewdribbling">78</div>
                        <div class="reviewshooting">75</div>
                        <div class="reviewdefending">32</div>
                        <div class="reviewpassing">72</div>
                        <div class="reviewphysical">71</div>
                    </div>
            </div>
        </div>
    </div>


        

    <?php include 'footer.php';?>
    
</body>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

</html>