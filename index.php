<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="favicon" type="img/ico" href="favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        
        
    </head>
    <body>
        
        <?php include 'header.php';?>
        <?php include 'mysql_connect.php';?>
        <?php include 'autocomplete.php';?>

        <?php

        // check if there's a player ID in the URL
        if ($_GET && $_GET["id"])
        {
            $playerID = $_GET["id"];
            $result = mysql_query("SELECT position FROM players WHERE id = '$playerID'");
            $row = mysql_fetch_row($result);
            $position = $row[0];

            // echo('<pre>');
            // print_r($row);
            // echo('</pre>');

            // echo ('position ' . $position);
            if ($position == 'GK')
            {
              include 'goalkeeper.php';  
            }  
            else
            {
                include 'player.php';
            } 
        } 
        else
        {
            include 'home.php';
        }    

        ?>

        <?php include 'footer.php';?>


        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
