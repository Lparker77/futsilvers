<!doctype html>
<html class="no-js" lang="">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="favicon" type="img/ico" href="favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
    </head>

<body>

        <?php include 'header.php';?>
        
        <div id="searchbox">
          SEARCH FOR SILVER PLAYERS TO VIEW STATS AND REVIEWS
          <hr>
            <form>
                <input type="text" name="firstname" id="search" placeholder="SEARCH FOR SILVER PLAYERS"><br>
            </form>
        </div>	

        <div class="reviewstemplatediv">

        	<div id="playerheader">
        		<div id="leftplayerheader">
        			<a href="reviews.php"><div class="backbutton">BACK TO REVIEWS</div></a>
        		</div>
            	<div id="centerplayerheader">
            		<div id="playername">BERTRAND TRAORÉ</div>
            		<div id="cardtype">PLAYER REVIEW</div>
            	</div>
            	<div id="rightplayerheader">
            		<div class="nextreviewbutton">NEXT REVIEW</div>
            	</div>
		</div><br>

        <div class="reviewbox1">
            <div class="reviewratingdiv">
                <div class="reviewuparrow">
                    <img id="greenarrow" src="img/greenarrow.png">
                </div>
                <div class="reviewscore">0</div>
                <div class="reviewdownarrow">
                    <img id="redarrow" src="img/redarrow.png">
                </div>
            </div>
            <div class="reviewinfodiv"><br>
                <div class="reviewtitle">"REVIEW TITLE HERE"</div>
                <div class="reviewusername">by Username</div><br>
                <div class="reviewdivmiddleinfo"><br>
                    <div class="reviewplayerquality">
                        <div class="playerqualityheader">PLAYER QUALITY</div>
                        <div class="playerqualitystars">
                            <div class="playerqualityvalue">5</div>
                            <img id="yellowstar1" src="img/yellowstar.png">
                            <img id="yellowstar2" src="img/yellowstar.png">
                            <img id="yellowstar3" src="img/yellowstar.png">
                            <img id="yellowstar4" src="img/yellowstar.png">
                            <img id="yellowstar5" src="img/yellowstar.png">
                            <img id="graystar1" src="img/graystar.png">
                            <img id="graystar2" src="img/graystar.png">
                            <img id="graystar3" src="img/graystar.png">
                            <img id="graystar4" src="img/graystar.png">
                            <img id="graystar5" src="img/graystar.png">
                        </div>
                    </div>
                    <div class="reviewpositiondiv">
                        <div class="playerpositionheader">POSITION</div>
                        <div class="playerposition">ST</div>
                    </div>
                    <div class="reviewformationused">
                        <div class="playerformationheader">FORMATION USED</div>
                        <div class="playerformation">4-3-3</div>
                    </div>
                </div>
            </div>
            <div class="reviewcarddiv">
                <div class="reviewcardarea">
                    <img id="reviewcard" src="img/cards/Silver-Rare.png">
                    <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                    <div id="reviewplayerrating">74</div>
                    <div class="reviewposition">ST</div>
                    <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                    <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                    <div class="reviewcardname">TRAORÉ</div>
                    <div class="reviewpacetag">PAC</div>
                    <div class="reviewdribblingtag">DRI</div>
                    <div class="reviewshootingtag">SHO</div>
                    <div class="reviewdefendingtag">DEF</div>
                    <div class="reviewpassingtag">PAS</div>
                    <div class="reviewphysicaltag">PHY</div>
                    <div class="reviewpace">85</div>
                    <div class="reviewdribbling">78</div>
                    <div class="reviewshooting">75</div>
                    <div class="reviewdefending">32</div>
                    <div class="reviewpassing">72</div>
                    <div class="reviewphysical">71</div>
                </div>
            </div>
        </div>

        <div class="pricepaidbox">
            <div class="pricepaidleft">
                <div class="boughtforheader">BOUGHT FOR</div>
                <div class="pricepaiddiv">
                    <div class="pricepaid">10,000</div>
                    <div class="coinimagediv"><img id="coinimage" src="img/coinimage.png"></div>
                </div>
            </div>
            <div class="pricepaidright">
                <div class="platformheader">PLATFORM</div>
                <div class="platformdiv">
                    <div class="platform">xbox</div>
                    <div class="platformimages">
                        <img id="xboxlogo" src="img/xboxlogo.png">
                        <img id="playstationlogo" src="img/playstationlogo.png">
                        <img id="originlogo" src="img/originlogo.png">
                    </div>
                </div>
            </div>
        </div>

        <div class="reviewsquadbox">
            <div class="squadboxheader">SQUAD USED IN</div>
            <div class="squadimagediv"><img id="squadimage" src="img/squadimage.png"></div>
            <div class="squaddescription">
                Bertrand Traoré is in my opinion the best BPL silver striker in the game. He's the perfect striker in a 4-1-2-1-2 formation alongside a much stronger striker like Agbonlahor, scoring 22 goals in 15 games for me. He's my favourite silver striker in the game and I'll explain why within this review!
            </div>
        </div>

        <div class="reviewpacebox">
            <div class="paceboxheader">PACE</div>
            <div class="reviewpaceingames">
                 <div class="reviewpaceingamesbox">
                    <div id="reviewpaceheader">
                        <div class="reviewstattext">PACE</div>
                        <div class="reviewpacenumber">85</div>
                    </div>
                    <div id="reviewsprintspeeddiv">
                        <div class="reviewsprintspeedtext">Sprint Speed</div>
                        <div class="reviewsprintspeednumber">83</div>
                    </div>
                     <div id="reviewaccelerationdiv">
                        <div class="accelerationtext">Acceleration</div>
                        <div class="reviewaccelerationnumber">87</div>
                    </div>
                </div>
            </div>
            <div class="squaddescription">
                Bertrand Traoré is in my opinion the best BPL silver striker in the game. He's the perfect striker in a 4-1-2-1-2 formation alongside a much stronger striker like Agbonlahor, scoring 22 goals in 15 games for me. He's my favourite silver striker in the game and I'll explain why within this review!
            </div>
        </div>

        <div class="reviewdribblingbox">
            <div class="paceboxheader">DRIBBLING</div>
            <div class="reviewdribblingingames">
                 <div class="reviewdribblingingamesbox">
                    <div id="reviewdribblingheader">
                        <div class="reviewstattext">DRIBBLING</div>
                        <div class="reviewdribblingnumber">78</div>
                    </div>
                    <div id="reviewdribblingstatdiv">
                        <div class="reviewdribblingstattext">Dribbling</div>
                        <div class="reviewdribblingstatnumber">77</div>
                    </div>
                    <div id="reviewballcontroldiv">
                        <div class="reviewballcontroltext">Ball Control</div>
                        <div class="reviewballcontrolnumber">80</div>
                    </div>
                    <div id="reviewreactionsdiv">
                        <div class="reviewreactionstext">Reactions</div>
                        <div class="reviewreactionsnumber">75</div>
                    </div>
                    <div id="reviewbalancediv">
                        <div class="reviewbalancetext">Balance</div>
                        <div class="reviewbalancenumber">74</div>
                    </div>
                    <div id="reviewagilitydiv">
                        <div class="reviewagilitytext">Agility</div>
                        <div class="reviewagilitynumber">82</div>
                    </div>   
                </div>
            </div>
            <div class="squaddescription">
                Bertrand Traoré is in my opinion the best BPL silver striker in the game. He's the perfect striker in a 4-1-2-1-2 formation alongside a much stronger striker like Agbonlahor, scoring 22 goals in 15 games for me. He's my favourite silver striker in the game and I'll explain why within this review!
            </div>
        </div>

        <div class="reviewshootingbox">
            <div class="paceboxheader">SHOOTING</div>
            <div class="reviewshootingingames">
                 <div class="reviewshootingingamesbox">
                    <div id="reviewshootingheader">
                        <div class="reviewstattext">SHOOTING</div>
                        <div class="reviewshootingnumber">75</div>
                    </div>
                    <div id="reviewpenaltiesdiv">
                        <div class="reviewpenaltiestext">Penalties</div>
                        <div class="reviewpenaltiesnumber">66</div>
                    </div>
                    <div id="reviewvolleysdiv">
                        <div class="reviewvolleystext">Volleys</div>
                        <div class="reviewvolleysnumber">76</div>
                    </div>
                    <div id="reviewlongshotsdiv">
                        <div class="reviewlongshotstext">Long Shots</div>
                        <div class="reviewlongshotsnumber">76</div>
                    </div>
                    <div id="reviewshotpowerdiv">
                        <div class="reviewshotpowertext">Shot Power</div>
                        <div class="reviewshotpowernumber">78</div>
                    </div>
                    <div id="reviewfinishingdiv">
                        <div class="reviewfinishingtext">Finishing</div>
                        <div class="reviewfinishingnumber">74</div>
                    </div>
                    <div id="reviewattpositioningdiv">
                        <div class="reviewattpositioningtext">Att. Positioning</div>
                        <div class="reviewattpositioningnumber">70</div>
                    </div>
                </div>
            </div>
            <div class="squaddescription">
                Bertrand Traoré is in my opinion the best BPL silver striker in the game. He's the perfect striker in a 4-1-2-1-2 formation alongside a much stronger striker like Agbonlahor, scoring 22 goals in 15 games for me. He's my favourite silver striker in the game and I'll explain why within this review!
            </div>
        </div>

        <div class="reviewdefendingbox">
            <div class="defendingboxheader">DEFENDING</div>
            <div class="reviewdefendingingames">
                 <div class="reviewdefendingingamesbox">
                    <div id="reviewdefendingheader">
                        <div class="reviewstattext">DEFENDING</div>
                        <div class="reviewdefendingnumber">32</div>
                    </div>
                    <div id="reviewinterceptionsdiv">
                        <div class="reviewinterceptionstext">Interceptions</div>
                        <div class="reviewinterceptionsnumber">39</div>
                    </div>
                    <div id="reviewheadingaccdiv">
                        <div class="reviewheadingacctext">Heading Acc.</div>
                        <div class="reviewheadingaccnumber">55</div>
                    </div>
                     <div id="reviewmarkingdiv">
                        <div class="reviewmarkingtext">Marking</div>
                        <div class="reviewmarkingnumber">30</div>
                    </div>
                    <div id="reviewstandingtacklediv">
                        <div class="reviewstandingtackletext">Standing Tackle</div>
                        <div class="reviewstandingtacklenumber">26</div>
                    </div>
                    <div id="reviewslidingtacklediv">
                        <div class="reviewslidingtackletext">Sliding Tackle</div>
                        <div class="reviewslidingtacklenumber">23</div>
                    </div>
                </div>
            </div>
            <div class="squaddescription">
                Bertrand Traoré is in my opinion the best BPL silver striker in the game. He's the perfect striker in a 4-1-2-1-2 formation alongside a much stronger striker like Agbonlahor, scoring 22 goals in 15 games for me. He's my favourite silver striker in the game and I'll explain why within this review!
            </div>
        </div>

        <div class="reviewpassingbox">
            <div class="passingboxheader">PASSING</div>
            <div class="reviewpassingingames">
                 <div class="reviewpassingingamesbox">
                    <div id="reviewpassingheader">
                        <div class="reviewstattext">PASSING</div>
                        <div class="reviewpassingnumber">72</div>
                    </div>
                    <div id="reviewcurvediv">
                        <div class="reviewcurvetext">Curve</div>
                        <div class="reviewcurvenumber">75</div>
                    </div>
                    <div id="reviewlongpassingdiv">
                        <div class="reviewlongpassingtext">Long Passing</div>
                        <div class="reviewlongpassingnumber">69</div>
                    </div>
                    <div id="reviewshortpassingdiv">
                        <div class="reviewshortpassingtext">Short Passing</div>
                        <div class="reviewshortpassingnumber">73</div>
                    </div>
                     <div id="reviewfreekickaccdiv">
                        <div class="reviewfreekickacctext">Free Kick Acc.</div>
                        <div class="reviewfreekickaccnumber">65</div>
                    </div>     
                    <div id="reviewcrossingdiv">
                        <div class="reviewcrossingtext">Crossing</div>
                        <div class="reviewcrossingnumber">69</div>
                    </div>
                    <div id="reviewvisiondiv">
                        <div class="reviewvisiontext">Vision</div>
                        <div class="reviewvisionnumber">74</div>
                    </div>
                </div>
            </div>
            <div class="squaddescription">
                Bertrand Traoré is in my opinion the best BPL silver striker in the game. He's the perfect striker in a 4-1-2-1-2 formation alongside a much stronger striker like Agbonlahor, scoring 22 goals in 15 games for me. He's my favourite silver striker in the game and I'll explain why within this review!
            </div>
        </div>

        <div class="reviewphysicalbox">
            <div class="physicalboxheader">PHYSICAL</div>
            <div class="reviewphysicalingames">
                 <div class="reviewphysicalingamesbox">
                    <div id="reviewphysicalheader">
                        <div class="reviewstattext">PHYSICAL</div>
                        <div class="reviewphysicalnumber">71</div>
                    </div>
                    <div id="reviewaggressiondiv">
                        <div class="reviewaggressiontext">Aggression</div>
                        <div class="reviewaggressionnumber">69</div>
                    </div>
                     <div id="reviewstrengthdiv">
                        <div class="reviewstrengthtext">Strength</div>
                        <div class="reviewstrengthnumber">71</div>
                    </div>
                    <div id="reviewstaminadiv">
                        <div class="reviewstaminatext">Stamina</div>
                        <div class="reviewstaminanumber">73</div>
                    </div>
                    <div id="reviewjumpingdiv">
                        <div class="reviewjumpingtext">Jumping</div>
                        <div class="reviewjumpingnumber">68</div>
                    </div>
                </div>
            </div>
            <div class="squaddescription">
                Bertrand Traoré is in my opinion the best BPL silver striker in the game. He's the perfect striker in a 4-1-2-1-2 formation alongside a much stronger striker like Agbonlahor, scoring 22 goals in 15 games for me. He's my favourite silver striker in the game and I'll explain why within this review!
            </div>
        </div>

        <div class="posnegbox">
            <div class="positives">
                <div class="positivesheader">POSITIVES</div>
                <div class="positivepoint">
                    <div class="positiveplus">+</div>
                    <div class="positivepointtext">Amazing shooting</div>
                </div>
                <div class="positivepoint">
                    <div class="positiveplus">+</div>
                    <div class="positivepointtext">Cheap</div>
                </div>
                <div class="positivepoint">
                    <div class="positiveplus">+</div>
                    <div class="positivepointtext">Incredibly fast</div>
                </div>
                <div class="positivepoint">
                    <div class="positiveplus">+</div>
                    <div class="positivepointtext">Can play in a number of positions</div>
                </div>
                <div class="positivepoint">
                    <div class="positiveplus">+</div>
                    <div class="positivepointtext">High dribbling</div>
                </div>
            </div>
            <div class="negatives">
                <div class="negativesheader">NEGATIVES</div>
                <div class="negativepoint">
                    <div class="negativeminus">-</div>
                    <div class="negativepointtext">Often pushed off the ball by defenders</div>
                </div>
                <div class="negativepoint">
                    <div class="negativeminus">-</div>
                    <div class="negativepointtext">Poor passing</div>
                </div>
                <div class="negativepoint">
                    <div class="negativeminus">-</div>
                    <div class="negativepointtext">Doesn't have many strong links</div>
                </div>
                <div class="negativepoint">
                    <div class="negativeminus">-</div>
                    <div class="negativepointtext">Inconsistent</div>
                </div>
            </div>
        </div><br>

        <div class="reviewcommentsection">
            <textarea type="text" rows="4" name="commentbox" id="commentbox" placeholder="How useful was the review? Leave a comment here..."></textarea>
            <input type="submit" value="SUBMIT" id="submitbutton">
        </div><br>

        <?php include 'footer.php';?>


</body>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/reviewtemplate.js"></script>

</html>