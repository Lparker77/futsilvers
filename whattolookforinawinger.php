

<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="whattolookforinwingerdiv">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltips.php"><div id="alltotwbutton">ALL TIPS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">what to look for in a silver winger</div>
                </div>
                <div id="righttotwheader">
                    <a href="whattolookforinastriker.php"><div id="nexttotwbutton">NEXT TIP</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="tipsbackground">
            
            <div class="totwplayersrow1">
                
                    
                    
                        <div class="tipsheaderimage">
                            <img id="cbheaderimg" src="img/wingersgraphic.png">
                        </div>
                        <div class="tipstext">
                           Silver wingers have always been my favourite players but the way I’ve chosen which wingers to use has changed in the last year or two. When I first started using silvers pace was the main stat I looked for in a winger. I still agree that pace is incredibly important but dribbling, shooting and passing are even more important in Fifa 16. The graphic above shows three silver wingers – one that should be used and will be used, another that probably won’t be used but should be and lastly one that will be used a lot but I won’t be using myself.<br><br>

Sinclair is the winger that should be used and is used. He looks to be the perfect winger with a balance of pace, dribbling and shooting. Generally, a top silver winger should always have 80+ pace, 80+ dribbling and 70+ shooting. Any winger with those stats will always be good and Sinclair is a perfect example. I’ve not included 4 star skills in the graphic as a key attribute to look out for because not everyone can do skills but if you can do skills, 4 star skills is a must as it takes a silver winger to the next level.<br><br>

Caio is the winger that should be used but isn't used by most. Although I consider 76 pace perfectly fine for a winger, the average silver user will dismiss him immediately because he doesn’t have 80+ pace. Personally I think wingers like Caio are well worth using in a 4-1-2-1-2 or  4-3-2-1 because they have the perfect blend of stats without the crazy pace. 75 dribbling, 77 shooting and 75 passing is amazing for a silver. I shouldn’t forget to mention that he’s 6’1″ as well. If you haven’t used slower wingers before then give him a go especially as Fifa 16 isn't as reliant on pace as previous years.<br><br>

Lastly, Damm is the winger who is used alot in LIGA MX silver teams online, but I will never use myself. Although he may have 95 pace, which don’t get me wrong I do see why people will use him, he doesn’t offer much else. His shooting and passing is poor and his dribbling is good but nothing special. Pace pretty much sums him up and I’d be disappointed if people use him over Brizuela who has 4 star skills and much better shooting ingame.
                        </div>
                    </div>
                
                   
            
           


        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>