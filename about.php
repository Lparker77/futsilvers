 <link rel="stylesheet" href="css/main.css">

<div id="aboutbackground"></div>

<div id="aboutdiv"><br>FUTSILVERS was created in 2016 by Lparker77, a longtime silver user who previously created silver based content on YouTube.<br><br>

Having used silvers for many years I wanted to create a new service tailored around the needs of a silver user, rather than the general FUT player.  <br><br>

While the features are fairly limited at the moment, I'm aiming to carry on improving this service after the release of FIFA 17.</div>