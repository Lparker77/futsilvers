<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="totw2div">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltotws.php"><div id="alltotwbutton">ALL TOTWS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">Team of the week 4 silvers</div>
                </div>
                <div id="righttotwheader">
                    
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="playercardinfosimilar">
            
            <div class="totwplayersrow1">
                <a href="index.php?id=81">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $gilletrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $gilletrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $gilletrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $gilletrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $gilletrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $gilletrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $gilletrow[1] ?>
                        </div>
                        <div class="reviewpacetag">DIV</div>
                        <div class="reviewdribblingtag">REF</div>
                        <div class="reviewshootingtag">HAN</div>
                        <div class="reviewdefendingtag">SPE</div>
                        <div class="reviewpassingtag">KIC</div>
                        <div class="reviewphysicaltag">POS</div>
                        <div class="reviewpace">
                                <?php echo $gilletrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $gilletrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $gilletrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $gilletrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $gilletrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $gilletrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $gilletrow[19] ?></div>
                            <div class="totwplayername"><?php echo $gilletrow[0] . '&nbsp' .  $gilletrow[1].',&nbsp'.$gilletrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Jean-Francois Gillet deservedly makes it into Team of the Week 4 after saving an astonishing 3 penalties in KV Mechelen's 1-0 win against Standard Liege.  <br><br>

                           While he isn't the tallest for a goalkeeper at 5'11" he looks like a great option for any Pro League or Belgium silver teams with his well rounded stats and most noticeably his 80 reflexes.  <br><br>

                           Although he'll be fairly popular, silver inform goalkeepers are generally fairly cheap so expect to pick him up for less than 10k at the weekend. 
                        </div>
                    </div>
                
                   
            </div>
           

           <div class="totwplayersrow2">
                <a href="index.php?id=82">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $matiprow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $matiprow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $matiprow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $matiprow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $matiprow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $matiprow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $matiprow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $matiprow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $matiprow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $matiprow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $matiprow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $matiprow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $matiprow[26] ?>
                        </div>
                    </div>
                </a>
                    
                   <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $matiprow[19] ?></div>
                            <div class="totwplayername"><?php echo $matiprow[0] . '&nbsp' .  $matiprow[1].',&nbsp'.$matiprow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Marvin Matip the Cameroon centre back scored 1 goal with a clean sheet for FC Ingolstadt last week.  <br><br>

                           He's the absolute dream for fans of Bundesliga silvers with amazing stats for a silver centre back. He looks like the perfect partner for Tah or Sorensen with 72 pace, 78 defending and 80 physical.  <br><br>

                           In previous years I would expect Matip to cost around 50k but this year with silver informs being fairly cheap and accessible, I expect him to be around 20k. 
                        </div>
                    </div>
                
                   
            </div>
               

               <div class="totwplayersrow1">
                <a href="index.php?id=84">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $necidrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $necidrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $necidrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $necidrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $necidrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $necidrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $necidrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $necidrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $necidrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $necidrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $necidrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $necidrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $necidrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $necidrow[19] ?></div>
                            <div class="totwplayername"><?php echo $necidrow[0] . '&nbsp' .  $necidrow[1].',&nbsp'.$necidrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Tomas Necid takes the third silver spot in this weeks Team of the Week with 3 goals and 1 assist for Bursaspor. <br><br>

                           I really like the look of this inform card as his non inform card is already a great target man. At 6'2" he's a great player to use next to El Kabir in a two striker formation.   <br><br>

                           Although he has 80 shooting and solid stats overall,  I expect his price to settle at around 10k by the weekend because of his low pace.
                        </div>
                    </div>
                
                   
            </div>


            <div class="totwplayersrow1">
                <a href="index.php?id=83">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $santosrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $santosrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $santosrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $santosrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $santosrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $santosrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $santosrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $santosrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $santosrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $santosrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $santosrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $santosrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $santosrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                   <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $santosrow[19] ?></div>
                            <div class="totwplayername"><?php echo $santosrow[0] . '&nbsp' .  $santosrow[1].',&nbsp'.$santosrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Santos is the last silver card in this weeks Team of the Week getting a brace of goals and assists for Suwon Bluewings. <br><br>

                           As a lover of Brazil silvers through the year this card looks incredible. He may be small but 78 shooting accompanied by 85 pace and 75 dribbling looks great for an attacking midfielder. <br><br>

                           I predict that Santos's price will probably start at 100k because he's Brazilian but gradually drop throughout the week and settle somewhere between 20-40k.
                        </div>
                    </div>
                
                   
            </div>

        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>