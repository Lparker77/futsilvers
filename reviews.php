<!doctype html>
<html class="no-js" lang="">
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="favicon" type="img/ico" href="favicon.ico">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        
    </head>

<body>

        <?php include 'header.php';?>
        
        <div id="searchbox">
          SEARCH FOR SILVER PLAYERS TO VIEW STATS AND REVIEWS
          <hr>
            <form>
                <input type="text" name="firstname" id="search" placeholder="SEARCH FOR SILVER PLAYERS"><br>
            </form>
        </div>	

        <div class="reviewsdiv">

        	<div id="playerheader">
        		<div id="leftplayerheader">
        			<a href="playerpage.php"><div id="reviewsbutton">PROFILE</div></a>
        			<a href="similar.php"><div id="similarbutton">SIMILAR</div></a>
        		</div>
            	<div id="centerplayerheader">
            		<div id="playername">PLAYER NAME</div>
            		<div id="cardtype">REVIEWS</div>
            	</div>
            	<div id="rightplayerheader">
                    <div id="thumbsdown"><img id="thumbs" src="img/thumbsdown.png"></div>
                    <div id="total">0</div>
                    <div id="thumbsup"><img id="thumbs" src="img/thumbsup.png"></div>
                </div>
		    </div>

		<div class="reviewbuttonsdiv">
            <div class="createreview">CREATE REVIEW</div>
            <div class="editreview">EDIT REVIEW</div>
        </div>

        <div class="reviewbox1">
            <div class="reviewratingdiv">
                <div class="reviewuparrow">
                    <img id="greenarrow" src="img/greenarrow.png">
                </div>
                <div class="reviewscore">0</div>
                <div class="reviewdownarrow">
                    <img id="redarrow" src="img/redarrow.png">
                </div>
            </div>
            <div class="reviewinfodiv">
                <div class="reviewtitle">"REVIEW TITLE HERE"</div>
                <div class="reviewusername">by Username</div><br>
                <div class="reviewdivmiddleinfo">
                    <div class="reviewplayerquality">
                        <div class="playerqualityheader">PLAYER QUALITY</div>
                        <div class="playerqualitystars">
                            <div class="playerqualityvalue">5</div>
                            <img id="yellowstar1" src="img/yellowstar.png">
                            <img id="yellowstar2" src="img/yellowstar.png">
                            <img id="yellowstar3" src="img/yellowstar.png">
                            <img id="yellowstar4" src="img/yellowstar.png">
                            <img id="yellowstar5" src="img/yellowstar.png">
                            <img id="graystar1" src="img/graystar.png">
                            <img id="graystar2" src="img/graystar.png">
                            <img id="graystar3" src="img/graystar.png">
                            <img id="graystar4" src="img/graystar.png">
                            <img id="graystar5" src="img/graystar.png">
                        </div>
                    </div>
                    <div class="reviewpositiondiv">
                        <div class="playerpositionheader">POSITION</div>
                        <div class="playerposition">ST</div>
                    </div>
                    <div class="reviewformationused">
                        <div class="playerformationheader">FORMATION USED</div>
                        <div class="playerformation">4-3-3</div>
                    </div>
                </div>
                <a href="reviewtemplate.php"><div class="readreview">READ REVIEW</div></a>
            </div>
            <div class="reviewcarddiv">
                <div class="reviewcardarea">
                    <img id="reviewcard" src="img/cards/Silver-Rare.png">
                    <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                    <div id="reviewplayerrating">74</div>
                    <div class="reviewposition">ST</div>
                    <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                    <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                    <div class="reviewcardname">TRAORÉ</div>
                    <div class="reviewpacetag">PAC</div>
                    <div class="reviewdribblingtag">DRI</div>
                    <div class="reviewshootingtag">SHO</div>
                    <div class="reviewdefendingtag">DEF</div>
                    <div class="reviewpassingtag">PAS</div>
                    <div class="reviewphysicaltag">PHY</div>
                    <div class="reviewpace">85</div>
                    <div class="reviewdribbling">78</div>
                    <div class="reviewshooting">75</div>
                    <div class="reviewdefending">32</div>
                    <div class="reviewpassing">72</div>
                    <div class="reviewphysical">71</div>
                </div>
            </div>
        </div><br>

        <div class="reviewbox2">
            <div class="reviewratingdiv">
                <div class="reviewuparrow">
                    <img id="greenarrow" src="img/greenarrow.png">
                </div>
                <div class="reviewscore">0</div>
                <div class="reviewdownarrow">
                    <img id="redarrow" src="img/redarrow.png">
                </div>
            </div>
            <div class="reviewinfodiv">
                <div class="reviewtitle">"REVIEW TITLE HERE"</div>
                <div class="reviewusername">by Username</div><br>
                <div class="reviewdivmiddleinfo">
                    <div class="reviewplayerquality">
                        <div class="playerqualityheader">PLAYER QUALITY</div>
                        <div class="playerqualitystars">
                            <div class="playerqualityvalue2">4</div>
                            <img id="yellowstar1review2" src="img/yellowstar.png">
                            <img id="yellowstar2review2" src="img/yellowstar.png">
                            <img id="yellowstar3review2" src="img/yellowstar.png">
                            <img id="yellowstar4review2" src="img/yellowstar.png">
                            <img id="yellowstar5review2" src="img/yellowstar.png">
                            <img id="graystar1review2" src="img/graystar.png">
                            <img id="graystar2review2" src="img/graystar.png">
                            <img id="graystar3review2" src="img/graystar.png">
                            <img id="graystar4review2" src="img/graystar.png">
                            <img id="graystar5review2" src="img/graystar.png">
                        </div>
                    </div>
                    <div class="reviewpositiondiv">
                        <div class="playerpositionheader">POSITION</div>
                        <div class="playerposition">ST</div>
                    </div>
                    <div class="reviewformationused">
                        <div class="playerformationheader">FORMATION USED</div>
                        <div class="playerformation">4-1-2-1-2</div>
                    </div>
                </div>
                <div class="readreview">READ REVIEW</div>
            </div>
            <div class="reviewcarddiv">
                <div class="reviewcardarea">
                    <img id="reviewcard" src="img/cards/Silver-Rare.png">
                    <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                    <div id="reviewplayerrating">74</div>
                    <div class="reviewposition">ST</div>
                    <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                    <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                    <div class="reviewcardname">TRAORÉ</div>
                    <div class="reviewpacetag">PAC</div>
                    <div class="reviewdribblingtag">DRI</div>
                    <div class="reviewshootingtag">SHO</div>
                    <div class="reviewdefendingtag">DEF</div>
                    <div class="reviewpassingtag">PAS</div>
                    <div class="reviewphysicaltag">PHY</div>
                    <div class="reviewpace">85</div>
                    <div class="reviewdribbling">78</div>
                    <div class="reviewshooting">75</div>
                    <div class="reviewdefending">32</div>
                    <div class="reviewpassing">72</div>
                    <div class="reviewphysical">71</div>
                </div>
            </div>
        </div><br>

        <div class="reviewbox3">
            <div class="reviewratingdiv">
                <div class="reviewuparrow">
                    <img id="greenarrow" src="img/greenarrow.png">
                </div>
                <div class="reviewscore">0</div>
                <div class="reviewdownarrow">
                    <img id="redarrow" src="img/redarrow.png">
                </div>
            </div>
            <div class="reviewinfodiv">
                <div class="reviewtitle">"REVIEW TITLE HERE"</div>
                <div class="reviewusername">by Username</div><br>
                <div class="reviewdivmiddleinfo">
                    <div class="reviewplayerquality">
                        <div class="playerqualityheader">PLAYER QUALITY</div>
                        <div class="playerqualitystars">
                            <div class="playerqualityvalue3">3</div>
                            <img id="yellowstar1review3" src="img/yellowstar.png">
                            <img id="yellowstar2review3" src="img/yellowstar.png">
                            <img id="yellowstar3review3" src="img/yellowstar.png">
                            <img id="yellowstar4review3" src="img/yellowstar.png">
                            <img id="yellowstar5review3" src="img/yellowstar.png">
                            <img id="graystar1review3" src="img/graystar.png">
                            <img id="graystar2review3" src="img/graystar.png">
                            <img id="graystar3review3" src="img/graystar.png">
                            <img id="graystar4review3" src="img/graystar.png">
                            <img id="graystar5review3" src="img/graystar.png">
                        </div>
                    </div>
                    <div class="reviewpositiondiv">
                        <div class="playerpositionheader">POSITION</div>
                        <div class="playerposition">CF</div>
                    </div>
                    <div class="reviewformationused">
                        <div class="playerformationheader">FORMATION USED</div>
                        <div class="playerformation">4-3-3(5)</div>
                    </div>
                </div>
                <div class="readreview">READ REVIEW</div>
            </div>
            <div class="reviewcarddiv">
                <div class="reviewcardarea">
                    <img id="reviewcard" src="img/cards/Silver-Rare.png">
                    <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                    <div id="reviewplayerrating">74</div>
                    <div class="reviewposition">ST</div>
                    <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                    <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                    <div class="reviewcardname">TRAORÉ</div>
                    <div class="reviewpacetag">PAC</div>
                    <div class="reviewdribblingtag">DRI</div>
                    <div class="reviewshootingtag">SHO</div>
                    <div class="reviewdefendingtag">DEF</div>
                    <div class="reviewpassingtag">PAS</div>
                    <div class="reviewphysicaltag">PHY</div>
                    <div class="reviewpace">85</div>
                    <div class="reviewdribbling">78</div>
                    <div class="reviewshooting">75</div>
                    <div class="reviewdefending">32</div>
                    <div class="reviewpassing">72</div>
                    <div class="reviewphysical">71</div>
                </div>
            </div>
        </div><br>

        <div class="reviewbox4">
            <div class="reviewratingdiv">
                <div class="reviewuparrow">
                    <img id="greenarrow" src="img/greenarrow.png">
                </div>
                <div class="reviewscore">0</div>
                <div class="reviewdownarrow">
                    <img id="redarrow" src="img/redarrow.png">
                </div>
            </div>
            <div class="reviewinfodiv">
                <div class="reviewtitle">"REVIEW TITLE HERE"</div>
                <div class="reviewusername">by Username</div><br>
                <div class="reviewdivmiddleinfo">
                    <div class="reviewplayerquality">
                        <div class="playerqualityheader">PLAYER QUALITY</div>
                        <div class="playerqualitystars">
                            <div class="playerqualityvalue4">1</div>
                            <img id="yellowstar1review4" src="img/yellowstar.png">
                            <img id="yellowstar2review4" src="img/yellowstar.png">
                            <img id="yellowstar3review4" src="img/yellowstar.png">
                            <img id="yellowstar4review4" src="img/yellowstar.png">
                            <img id="yellowstar5review4" src="img/yellowstar.png">
                            <img id="graystar1review4" src="img/graystar.png">
                            <img id="graystar2review4" src="img/graystar.png">
                            <img id="graystar3review4" src="img/graystar.png">
                            <img id="graystar4review4" src="img/graystar.png">
                            <img id="graystar5review4" src="img/graystar.png">
                        </div>
                    </div>
                    <div class="reviewpositiondiv">
                        <div class="playerpositionheader">POSITION</div>
                        <div class="playerposition">CAM</div>
                    </div>
                    <div class="reviewformationused">
                        <div class="playerformationheader">FORMATION USED</div>
                        <div class="playerformation">4-2-2-2</div>
                    </div>
                </div>
                <div class="readreview">READ REVIEW</div>
            </div>
            <div class="reviewcarddiv">
                <div class="reviewcardarea">
                    <img id="reviewcard" src="img/cards/Silver-Rare.png">
                    <div id="reviewplayerimgdiv"><img id="reviewplayerimg" src="img/photo/Traore.png"></div>
                    <div id="reviewplayerrating">74</div>
                    <div class="reviewposition">ST</div>
                    <div class="reviewplayerclub"><img id="reviewbadge" src="img/badge/Chelsea.png"></div>
                    <div class="reviewplayerflag"><img id="reviewflag" src="img/flag/BurkinaFaso.png"></div>
                    <div class="reviewcardname">TRAORÉ</div>
                    <div class="reviewpacetag">PAC</div>
                    <div class="reviewdribblingtag">DRI</div>
                    <div class="reviewshootingtag">SHO</div>
                    <div class="reviewdefendingtag">DEF</div>
                    <div class="reviewpassingtag">PAS</div>
                    <div class="reviewphysicaltag">PHY</div>
                    <div class="reviewpace">85</div>
                    <div class="reviewdribbling">78</div>
                    <div class="reviewshooting">75</div>
                    <div class="reviewdefending">32</div>
                    <div class="reviewpassing">72</div>
                    <div class="reviewphysical">71</div>
                </div>
            </div>
        </div><br><br>

       <?php include 'footer.php';?>

</body>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

</html>