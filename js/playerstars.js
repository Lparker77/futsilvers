// Change colour of card text depending on card type


$(function() {  
    if ($('#informplayerheader:contains("Inform Silver Players")').length > 0) {
        $('.rowcardname, .reviewpacetag, .reviewpace, .reviewdribblingtag, .reviewdribbling, .reviewshootingtag, .reviewshooting, .reviewdefendingtag, .reviewdefending, .reviewpassingtag, .reviewpassing, .reviewphysicaltag, .reviewphysical').css('color', '#dce5eb');
    }
});

$(function() {  
    if ($('#informplayerheader:contains("All Silver Players")').length > 0) {
        $('.ifrowcardname, .ifreviewpacetag, .ifreviewpace, .ifreviewdribblingtag, .ifreviewdribbling, .ifreviewshootingtag, .ifreviewshooting, .ifreviewdefendingtag, .ifreviewdefending, .ifreviewpassingtag, .ifreviewpassing, .ifreviewphysicaltag, .ifreviewphysical').css('color', '#dce5eb');
    }
});

$(function() {  
    if ($('#informplayerheader:contains("All Silver Players")').length > 0) {
        $('.rowcardname, .reviewpacetag, .reviewpace, .reviewdribblingtag, .reviewdribbling, .reviewshootingtag, .reviewshooting, .reviewdefendingtag, .reviewdefending, .reviewpassingtag, .reviewpassing, .reviewphysicaltag, .reviewphysical').css('color', 'black');
    }
});

$(function() {  
    if ($('#informplayerheader:contains("All 5 star skillers")').length > 0) {
        $('.rowcardname, .reviewpacetag, .reviewpace, .reviewdribblingtag, .reviewdribbling, .reviewshootingtag, .reviewshooting, .reviewdefendingtag, .reviewdefending, .reviewpassingtag, .reviewpassing, .reviewphysicaltag, .reviewphysical').css('color', 'black');
    }
});

$(function() {  
    if ($('#informplayerheader:contains("All 5 Star Skillers")').length > 0) {
        $('.rowcardname, .reviewpacetag, .reviewpace, .reviewdribblingtag, .reviewdribbling, .reviewshootingtag, .reviewshooting, .reviewdefendingtag, .reviewdefending, .reviewpassingtag, .reviewpassing, .reviewphysicaltag, .reviewphysical').css('color', 'black');
    }
});

$(function() {  
    if ($('.cardtype:contains("Inform Silver")').length > 0) {
        $('#cardname, #pacetag, #pace, #dribblingtag, #dribbling, #shootingtag, #shooting, #defendingtag, #defending, #passingtag, #passing, #physicaltag, #physical').css('color', '#dce5eb');
    }
});

$(function() {  
    if ($('#informplayerheader:contains("Team of the week")').length > 0) {
        $('.rowcardname, .reviewpacetag, .reviewpace, .reviewdribblingtag, .reviewdribbling, .reviewshootingtag, .reviewshooting, .reviewdefendingtag, .reviewdefending, .reviewpassingtag, .reviewpassing, .reviewphysicaltag, .reviewphysical').css('color', '#dce5eb');
    }
});




// Display number of stars based on skill move value

if ($(".skillmovesvaluediv").text() == 1) {
    document.getElementById("skillmoves1").style.display = "inline-block";
}
else
if ($(".skillmovesvaluediv").text() == 2) {
    document.getElementById("skillmoves2").style.display = "inline-block";
}
else
if ($(".skillmovesvaluediv").text() == 3) {
    document.getElementById("skillmoves3").style.display = "inline-block";
}
else
if ($(".skillmovesvaluediv").text() == 4) {
    document.getElementById("skillmoves4").style.display = "inline-block";
}
else
if ($(".skillmovesvaluediv").text() == 5) {
    document.getElementById("skillmoves5").style.display = "inline-block";
}

// Display number of stars based on weak foot value

if ($(".weakfootvaluediv").text() == 1) {
    document.getElementById("weakfoot1").style.display = "inline-block";
}
else
if ($(".weakfootvaluediv").text() == 2) {
    document.getElementById("weakfoot2").style.display = "inline-block";
}
else
if ($(".weakfootvaluediv").text() == 3) {
    document.getElementById("weakfoot3").style.display = "inline-block";
}
else
if ($(".weakfootvaluediv").text() == 4) {
    document.getElementById("weakfoot4").style.display = "inline-block";
}
else
if ($(".weakfootvaluediv").text() == 5) {
    document.getElementById("weakfoot5").style.display = "inline-block";
}



