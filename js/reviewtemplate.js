/* Change background colour of div depending on number */
/* Review Headers */

if ($(".reviewpacenumber").text() <= 59) {
    document.getElementById("reviewpaceheader").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewpacenumber").text() < 80) {
    document.getElementById("reviewpaceheader").style.backgroundColor = "#edb611";
}
else
if ($(".reviewpacenumber").text() >= 80) {
    document.getElementById("reviewpaceheader").style.backgroundColor = "#5fc042";
}



if ($(".reviewdribblingnumber").text() <= 59) {
    document.getElementById("reviewdribblingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewdribblingnumber").text() < 80) {
    document.getElementById("reviewdribblingheader").style.backgroundColor = "#edb611";
}
else
if ($(".reviewdribblingnumber").text() >= 80) {
    document.getElementById("reviewdribblingheader").style.backgroundColor = "#5fc042";
}



if ($(".reviewdefendingnumber").text() <= 59) {
    document.getElementById("reviewdefendingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewdefendingnumber").text() < 80) {
    document.getElementById("reviewdefendingheader").style.backgroundColor = "#edb611";
}
else
if ($(".reviewdefendingnumber").text() >= 80) {
    document.getElementById("reviewdefendingheader").style.backgroundColor = "#5fc042";
}



if ($(".reviewpassingnumber").text() <= 59) {
    document.getElementById("reviewpassingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewpassingnumber").text() < 80) {
    document.getElementById("reviewpassingheader").style.backgroundColor = "#edb611";
}
else
if ($(".reviewpassingnumber").text() >= 80) {
    document.getElementById("reviewpassingheader").style.backgroundColor = "#5fc042";
}



if ($(".reviewphysicalnumber").text() <= 59) {
    document.getElementById("reviewphysicalheader").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewphysicalnumber").text() < 80) {
    document.getElementById("reviewphysicalheader").style.backgroundColor = "#edb611";
}
else
if ($(".reviewphysicalnumber").text() >= 80) {
    document.getElementById("reviewphysicalheader").style.backgroundColor = "#5fc042";
}



/* Review Pace */

if ($(".reviewaccelerationnumber").text() <= 59) {
    document.getElementById("reviewaccelerationdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewaccelerationnumber").text() < 80) {
    document.getElementById("reviewaccelerationdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewaccelerationnumber").text() >= 80) {
    document.getElementById("reviewaccelerationdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewsprintspeednumber").text() <= 59) {
    document.getElementById("reviewsprintspeeddiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewsprintspeednumber").text() < 80) {
    document.getElementById("reviewsprintspeeddiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewsprintspeednumber").text() >= 80) {
    document.getElementById("reviewsprintspeeddiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewshootingnumber").text() <= 59) {
    document.getElementById("reviewshootingheader").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewshootingnumber").text() < 80) {
    document.getElementById("reviewshootingheader").style.backgroundColor = "#edb611";
}
else
if ($(".reviewshootingnumber").text() >= 80) {
    document.getElementById("reviewshootingheader").style.backgroundColor = "#5fc042";
}



/* Review Dribbling */

if ($(".reviewagilitynumber").text() <= 59) {
    document.getElementById("reviewagilitydiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewagilitynumber").text() < 80) {
    document.getElementById("reviewagilitydiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewagilitynumber").text() >= 80) {
    document.getElementById("reviewagilitydiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewbalancenumber").text() <= 59) {
    document.getElementById("reviewbalancediv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewbalancenumber").text() < 80) {
    document.getElementById("reviewbalancediv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewbalancenumber").text() >= 80) {
    document.getElementById("reviewbalancediv").style.backgroundColor = "#5fc042";
}



if ($(".reviewreactionsnumber").text() <= 59) {
    document.getElementById("reviewreactionsdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewreactionsnumber").text() < 80) {
    document.getElementById("reviewreactionsdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewreactionsnumber").text() >= 80) {
    document.getElementById("reviewreactionsdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewballcontrolnumber").text() <= 59) {
    document.getElementById("reviewballcontroldiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewballcontrolnumber").text() < 80) {
    document.getElementById("reviewballcontroldiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewballcontrolnumber").text() >= 80) {
    document.getElementById("reviewballcontroldiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewdribblingstatnumber").text() <= 59) {
    document.getElementById("reviewdribblingstatdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewdribblingstatnumber").text() < 80) {
    document.getElementById("reviewdribblingstatdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewdribblingstatnumber").text() >= 80) {
    document.getElementById("reviewdribblingstatdiv").style.backgroundColor = "#5fc042";
}



/* Shooting */

if ($(".reviewattpositioningnumber").text() <= 59) {
    document.getElementById("reviewattpositioningdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewattpositioningnumber").text() < 80) {
    document.getElementById("reviewattpositioningdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewattpositioningnumber").text() >= 80) {
    document.getElementById("reviewattpositioningdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewfinishingnumber").text() <= 59) {
    document.getElementById("reviewfinishingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewfinishingnumber").text() < 80) {
    document.getElementById("reviewfinishingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewfinishingnumber").text() >= 80) {
    document.getElementById("reviewfinishingdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewshotpowernumber").text() <= 59) {
    document.getElementById("reviewshotpowerdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewshotpowernumber").text() < 80) {
    document.getElementById("reviewshotpowerdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewshotpowernumber").text() >= 80) {
    document.getElementById("reviewshotpowerdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewlongshotsnumber").text() <= 59) {
    document.getElementById("reviewlongshotsdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewlongshotsnumber").text() < 80) {
    document.getElementById("reviewlongshotsdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewlongshotsnumber").text() >= 80) {
    document.getElementById("reviewlongshotsdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewvolleysnumber").text() <= 59) {
    document.getElementById("reviewvolleysdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewvolleysnumber").text() < 80) {
    document.getElementById("reviewvolleysdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewvolleysnumber").text() >= 80) {
    document.getElementById("reviewvolleysdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewpenaltiesnumber").text() <= 59) {
    document.getElementById("reviewpenaltiesdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewpenaltiesnumber").text() < 80) {
    document.getElementById("reviewpenaltiesdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewpenaltiesnumber").text() >= 80) {
    document.getElementById("reviewpenaltiesdiv").style.backgroundColor = "#5fc042";
}



/* Defending */

if ($(".reviewinterceptionsnumber").text() <= 59) {
    document.getElementById("reviewinterceptionsdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewinterceptionsnumber").text() < 80) {
    document.getElementById("reviewinterceptionsdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewinterceptionsnumber").text() >= 80) {
    document.getElementById("reviewinterceptionsdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewheadingaccnumber").text() <= 59) {
    document.getElementById("reviewheadingaccdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewheadingaccnumber").text() < 80) {
    document.getElementById("reviewheadingaccdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewheadingaccnumber").text() >= 80) {
    document.getElementById("reviewheadingaccdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewmarkingnumber").text() <= 59) {
    document.getElementById("reviewmarkingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewmarkingnumber").text() < 80) {
    document.getElementById("reviewmarkingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewmarkingnumber").text() >= 80) {
    document.getElementById("reviewmarkingdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewstandingtacklenumber").text() <= 59) {
    document.getElementById("reviewstandingtacklediv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewstandingtacklenumber").text() < 80) {
    document.getElementById("reviewstandingtacklediv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewstandingtacklenumber").text() >= 80) {
    document.getElementById("reviewstandingtacklediv").style.backgroundColor = "#5fc042";
}



if ($(".reviewslidingtacklenumber").text() <= 59) {
    document.getElementById("reviewslidingtacklediv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewslidingtacklenumber").text() < 80) {
    document.getElementById("reviewslidingtacklediv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewslidingtacklenumber").text() >= 80) {
    document.getElementById("reviewslidingtacklediv").style.backgroundColor = "#5fc042";
}



/* Passing */

if ($(".reviewvisionnumber").text() <= 59) {
    document.getElementById("reviewvisiondiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewvisionnumber").text() < 80) {
    document.getElementById("reviewvisiondiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewvisionnumber").text() >= 80) {
    document.getElementById("reviewvisiondiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewcrossingnumber").text() <= 59) {
    document.getElementById("reviewcrossingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewcrossingnumber").text() < 80) {
    document.getElementById("reviewcrossingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewcrossingnumber").text() >= 80) {
    document.getElementById("reviewcrossingdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewfreekickaccnumber").text() <= 59) {
    document.getElementById("reviewfreekickaccdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewfreekickaccnumber").text() < 80) {
    document.getElementById("reviewfreekickaccdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewfreekickaccnumber").text() >= 80) {
    document.getElementById("reviewfreekickaccdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewshortpassingnumber").text() <= 59) {
    document.getElementById("reviewshortpassingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewshortpassingnumber").text() < 80) {
    document.getElementById("reviewshortpassingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewshortpassingnumber").text() >= 80) {
    document.getElementById("reviewshortpassingdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewlongpassingnumber").text() <= 59) {
    document.getElementById("reviewlongpassingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewlongpassingnumber").text() < 80) {
    document.getElementById("reviewlongpassingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewlongpassingnumber").text() >= 80) {
    document.getElementById("reviewlongpassingdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewcurvenumber").text() <= 59) {
    document.getElementById("reviewcurvediv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewcurvenumber").text() < 80) {
    document.getElementById("reviewcurvediv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewcurvenumber").text() >= 80) {
    document.getElementById("reviewcurvediv").style.backgroundColor = "#5fc042";
}

/* Physical */

if ($(".reviewjumpingnumber").text() <= 59) {
    document.getElementById("reviewjumpingdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewjumpingnumber").text() < 80) {
    document.getElementById("reviewjumpingdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewjumpingnumber").text() >= 80) {
    document.getElementById("reviewjumpingdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewstaminanumber").text() <= 59) {
    document.getElementById("reviewstaminadiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewstaminanumber").text() < 80) {
    document.getElementById("reviewstaminadiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewstaminanumber").text() >= 80) {
    document.getElementById("reviewstaminadiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewstrengthnumber").text() <= 59) {
    document.getElementById("reviewstrengthdiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewstrengthnumber").text() < 80) {
    document.getElementById("reviewstrengthdiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewstrengthnumber").text() >= 80) {
    document.getElementById("reviewstrengthdiv").style.backgroundColor = "#5fc042";
}



if ($(".reviewaggressionnumber").text() <= 59) {
    document.getElementById("reviewaggressiondiv").style.backgroundColor = "#de2323";
} 
else
if ($(".reviewaggressionnumber").text() < 80) {
    document.getElementById("reviewaggressiondiv").style.backgroundColor = "#edb611";
}
else
if ($(".reviewaggressionnumber").text() >= 80) {
    document.getElementById("reviewaggressiondiv").style.backgroundColor = "#5fc042";
}