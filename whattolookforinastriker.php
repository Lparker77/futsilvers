

<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="whattolookforinstrikerdiv">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltips.php"><div id="alltotwbutton">ALL TIPS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">what to look for in a silver striker</div>
                </div>
                <div id="righttotwheader">
                    <a href="whattolookforinacb.php"><div id="nexttotwbutton">NEXT TIP</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="tipsbackground">
            
            <div class="totwplayersrow1">
                
                    
                    
                        <div class="tipsheaderimage">
                            <img id="cbheaderimg" src="img/strikersgraphic.png">
                        </div>
                        <div class="tipstext">
                           Along with wingers, strikers are what makes silver teams so fun to use. There have always been amazing silver strikers and this year is no exception. With the other positions I showed two players that should be used and one that shouldn’t. For the strikers I’m just going to talk about three types of striker that should be used as I couldn’t find many examples that shouldn’t be used! I’ve tried to get the point across that pace shouldn’t be the determining factor when picking players and while strikers are the similar, pace is still a key stat behind shooting. Over the last few years I’ve always looked at 70+ shooting as the benchmark for a decent silver striker so anything over 70 and you can’t go wrong.<br><br>

Muriel was possibly my favourite silver striker in FIFA 15 and is almost identical in Fifa 16. With pace, dribbling and shooting he's one of the best non inform silver strikers in Fifa 16. I would argue that he’s almost perfect with high/low workrates and 4 star kills as well. My advice if you can’t play without a fast striker like Muriel is to make sure you choose a player with at least 70 shooting.<br><br>

Almeida is the complete opposite to Muriel and many choose to ignore him completely because of his low pace and dribbling. He may be slow but some of my favourite silver strikers this year are target men, not to abuse crosses but more to abuse their strength and shooting. Almeida is an awesome target man with 78 shooting and 82 physical and is great ingame with a larger emphasis on positioning and strength which he has in abundance.<br><br>

Lastly, the all round striker Mora. Mora is one of those strikers that has largely gone unnoticed this year with all the pace in the Argentine league. He has some of the best all round stats for a silver striker – enough pace, amazing shooting stats and solid dribbling. He also has 4 star skills along with 73 heading accuracy hidden away in his ingame stats.
                        </div>
                    </div>
                
                   
            
           


        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>