

<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="whattolookforincbdiv">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltips.php"><div id="alltotwbutton">ALL TIPS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">what to look for in a silver CB</div>
                </div>
                <div id="righttotwheader">
                    <a href="whattolookforinafullback.php"><div id="nexttotwbutton">NEXT TIP</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="tipsbackground">
            
            <div class="totwplayersrow1">
                
                    
                    
                        <div class="tipsheaderimage">
                            <img id="cbheaderimg" src="img/defendersgraphic.png">
                        </div>
                        <div class="tipstext">
                           Some of the most important players in a silver team are the central defenders. Without good centre backs your team will get ripped apart by the average pace team so I want to explain how I’ll be choosing my defenders this year. <br><br>

To the average online opponent or to players that may not have used silvers before, there’s usually only one stat that seems to matter. Pace. Obviously pace is great to have but in my opinion, pure pace won’t work this year because heading and positioning play a much more integral part. As a result you should be looking out for a lot more than pace when selecting players for your team. The graphic above shows 3 completely different types of centre back, two you should be looking to use and one you shouldn’t. It also shows the four things I look for the most when choosing my centre backs.<br><br>

The first centre back is Kverkvelia from the Russian League. This is what I would call the perfect silver centre back because he has all four of the attributes I’m looking for. He’s 6’5″, has great Defending and Physical stats, Medium/Medium workrates and obviously great pace.<br><br>

The second centre back is Sorensen from the Bundesliga. This is the type of centre back you’ll probably end up using in most cases as you won’t be able to fit an all round centre back like Kvervelia in every team. Sorensen may only have 62 pace but he has incredibly solid Defending and Physical stats for a silver, perfect low/high workrates and he’s 6’4″. If you’re the type of player worries about pace look at using centre backs with at least 60 pace and good defensive stats.<br><br>

Lastly we have Asare from the Pro League. This type of centre back in a absolute no go this year due to heading and positioning. You’ll probably still see him used a lot because of his 78 pace but other than that, he’ll offer you nothing with his height, defensive stats and workrates.<br><br>

If you take just one thing from this section of the guide then it should be that a slow centre back with amazing defensive stats will be far better than a fast player with poor stats. 
                        </div>
                    </div>
                
                   
            
           


        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>