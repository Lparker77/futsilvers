<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="totw3div">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltotws.php"><div id="alltotwbutton">ALL TOTWS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">Team of the week 3 silvers</div>
                </div>
                <div id="righttotwheader">
                    <a href="totw4.php"><div id="nexttotwbutton">NEXT TOTW</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="playercardinfosimilar">
            
            <div class="totwplayersrow1">
                <a href="index.php?id=80">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $kalinicrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $kalinicrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $kalinicrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $kalinicrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $kalinicrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $kalinicrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $kalinicrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $kalinicrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $kalinicrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $kalinicrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $kalinicrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $kalinicrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $kalinicrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $kalinicrow[19] ?></div>
                            <div class="totwplayername"><?php echo $kalinicrow[0] . '&nbsp' .  $kalinicrow[1].',&nbsp'.$kalinicrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Nikola Kalinic scored a brace against Sassuolo with a brilliant performance for Fiorentina. <br><br>

                           His inform card makes an already great looking silver card even better with a boost to his dribbling, shooting and pace stats. He looks like a great alternative for Serie A silver sides and definitely worth using as a target man alongside other Serie A striker like Muriel or Niang. <br><br>

                           I don't expect him to cost much more than 20k during the week with the chance of rising in price in the future because he plays in a very popular league.
                        </div>
                    </div>
                
                   
            </div>
           

           <div class="totwplayersrow2">
                <a href="index.php?id=78">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $squillacirow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $squillacirow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $squillacirow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $squillacirow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $squillacirow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $squillacirow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $squillacirow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $squillacirow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $squillacirow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $squillacirow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $squillacirow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $squillacirow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $squillacirow[26] ?>
                        </div>
                    </div>
                </a>
                    
                   <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $squillacirow[19] ?></div>
                            <div class="totwplayername"><?php echo $squillacirow[0] . '&nbsp' .  $squillacirow[1].',&nbsp'.$squillacirow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                            The former Arsenal man Sebastien Squillaci makes it into Team of the Week 3 with a goal and a clean sheet for SC Bastia. <br><br>

                           He's definitey not a card for everyone with his 37 pace at centre back but could be used by some silver users if you can see past his pace and use him for his 80 defending. <br><br>

                           His pace alone will make him very cheap, probably around 10k with the chance of rising in the future because of his popular league and nation.
                        </div>
                    </div>
                
                   
            </div>
               

               <div class="totwplayersrow1">
                <a href="index.php?id=79">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $larinrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $larinrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $larinrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $larinrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $larinrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $larinrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $larinrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $larinrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $larinrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $larinrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $larinrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $larinrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $larinrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $larinrow[19] ?></div>
                            <div class="totwplayername"><?php echo $larinrow[0] . '&nbsp' .  $larinrow[1].',&nbsp'.$larinrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Cyle Larin makes it into this weeks Team of the Week as the first MLS player to receive an inform card this year, scoring a hat-trick for Orlando City. <br><br>

                           This looks like an interesting card if you ignore his low rating and look more towards his height. At 6'2" he looks like a nice target man.<br><br>

                           Its difficult to guess his price as MLS silver players are generally very popular but I expect him to be cheap because of his low rating.
                        </div>
                    </div>
                
                   
            </div>


           

        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>