<?php
// PDO connect *********
function connect() {
    return new PDO('mysql:host=localhost;dbname=futsilvers_database', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}

$pdo = connect();
$keyword = '%'.$_POST['keyword'].'%';
$sql = "SELECT * FROM players WHERE surname LIKE (:keyword) ORDER BY overallrating DESC LIMIT 0, 10";
$query = $pdo->prepare($sql);
$query->bindParam(':keyword', $keyword, PDO::PARAM_STR);
$query->execute();
$list = $query->fetchAll();
foreach ($list as $rs) {
	// put in bold the written text
	$surname = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $rs['Surname']);
	// add new option
    echo '<li><a href="?id=' . $rs['ID'] . '">'.$surname.'</a></li>';
}
?>