<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="totw2div">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltotws.php"><div id="alltotwbutton">ALL TOTWS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">Team of the week 2 silvers</div>
                </div>
                <div id="righttotwheader">
                    <a href="totw3.php"><div id="nexttotwbutton">NEXT TOTW</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="playercardinfosimilar">
            
            <div class="totwplayersrow1">
                <a href="index.php?id=74">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $izaguirrerow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $izaguirrerow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $izaguirrerow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $izaguirrerow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $izaguirrerow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $izaguirrerow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $izaguirrerow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $izaguirrerow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $izaguirrerow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $izaguirrerow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $izaguirrerow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $izaguirrerow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $izaguirrerow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $izaguirrerow[19] ?></div>
                            <div class="totwplayername"><?php echo $izaguirrerow[0] . '&nbsp' .  $izaguirrerow[1].',&nbsp'.$izaguirrerow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Featuring in Team of the Week 2 is Emilio Izaguirre from Celtic who bagged himself 2 goals from the left back position over the weekend. <br><br>

                           His non inform card was already the best option at left back for Scottish League silver teams so I expect to see this card a lot. <br><br>

                           Celtic inform cards are usually fairly sought after if previous years are to go by. He could cost anything from 10-30k depending on his rarity.
                        </div>
                    </div>
                
                   
            </div>
           

           <div class="totwplayersrow2">
                <a href="index.php?id=75">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $ndiayerow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $ndiayerow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $ndiayerow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $ndiayerow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $ndiayerow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $ndiayerow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $ndiayerow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $ndiayerow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $ndiayerow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $ndiayerow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $ndiayerow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $ndiayerow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $ndiayerow[26] ?>
                        </div>
                    </div>
                </a>
                    
                   <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $ndiayerow[19] ?></div>
                            <div class="totwplayername"><?php echo $ndiayerow[0] . '&nbsp' .  $ndiayerow[1].',&nbsp'.$ndiayerow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Papa Ndiaye takes the second silver spot in this weeks Team of the Week with 2 goals and 1 assist from the central midfield position for his team Osmanlispor. <br><br>

                           I really like the look of this inform card as it's incredibly well rounded and adds another great option for the already stacked Turkish League silvers. He definitely look like he's worth trying out anywhere across the range of central midfield positions. <br><br>

                           As he's only 71 rated and not very well known I don't expect this card to be too expensive, probably around the 10-15k mark.
                        </div>
                    </div>
                
                   
            </div>
               

               <div class="totwplayersrow1">
                <a href="index.php?id=76">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $towellrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $towellrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $towellrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $towellrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $towellrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $towellrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $towellrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $towellrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $towellrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $towellrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $towellrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $towellrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $towellrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                    <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $towellrow[19] ?></div>
                            <div class="totwplayername"><?php echo $towellrow[0] . '&nbsp' .  $towellrow[1].',&nbsp'.$towellrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Richie Towell from Dundalk is an unusual selection with 1 goal from defensive midfield, but then again, stats don't tell you everything about his performance. <br><br>

                           While I do like the look of his stats for a midfielder with 77 physical and 70 shooting the stand out stats, I won't be using him as the Irish League is not a great league for silver teams. <br><br>

                           Because of his league and low rating I'd be surprised if he rose to anymore than 10k.
                        </div>
                    </div>
                
                   
            </div>


            <div class="totwplayersrow1">
                <a href="index.php?id=77">
                    <div class="rowcard1">
                        <img id="similarcard" <?php echo $kadahrow[18] ?>
                        <div id="reviewplayerimgdiv"><img id="reviewplayerimg" <?php echo $kadahrow[2] ?></div>
                        <div id="reviewplayerrating">
                                <?php echo $kadahrow[19] ?>
                        </div>
                        <div class="reviewposition">
                                <?php echo $kadahrow[20] ?>
                        </div>
                        <div class="reviewplayerclub"><img id="reviewbadge" <?php echo $kadahrow[4] ?></div>
                        <div class="reviewplayerflag"><img id="reviewflag" <?php echo $kadahrow[8] ?></div>
                        <div class="rowcardname">
                                <?php echo $kadahrow[1] ?>
                        </div>
                        <div class="reviewpacetag">PAC</div>
                        <div class="reviewdribblingtag">DRI</div>
                        <div class="reviewshootingtag">SHO</div>
                        <div class="reviewdefendingtag">DEF</div>
                        <div class="reviewpassingtag">PAS</div>
                        <div class="reviewphysicaltag">PHY</div>
                        <div class="reviewpace">
                                <?php echo $kadahrow[21] ?>
                        </div>
                        <div class="reviewdribbling">
                                <?php echo $kadahrow[24] ?>
                        </div>
                        <div class="reviewshooting">
                                <?php echo $kadahrow[22] ?>
                        </div>
                        <div class="reviewdefending">
                                <?php echo $kadahrow[25] ?>
                        </div>
                        <div class="reviewpassing">
                                <?php echo $kadahrow[23] ?>
                        </div>
                        <div class="reviewphysical">
                                <?php echo $kadahrow[26] ?>
                        </div>
                    </div>
                </a>
                    
                   <div class="totwplayerdescription">
                        <div class="totwplayerdescriptionheader">
                            <div class="totwplayerovrrating"><?php echo $kadahrow[19] ?></div>
                            <div class="totwplayername"><?php echo $kadahrow[0] . '&nbsp' .  $kadahrow[1].',&nbsp'.$kadahrow[3] ?></div>
                        </div>
                        <div class="totwplayertext">
                           Deniz Kadah makes it into Team of the Week two as the second player from the Turkish League scoring 3 goals at the weekend. <br><br>

                           I don't like the look of this card at all because his stats look incredibly poor for an inform card. There's also no reason to use him over the likes of El Kabir or Necid.  <br><br>

                           Another card that won't be used much so I'm not expecting him to be much more than discard price at 6-10k.
                        </div>
                    </div>
                
                   
            </div>

        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>