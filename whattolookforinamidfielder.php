

<?php include 'header.php';?>
<?php include 'mysql_connect.php';?>
<?php include 'pullsilverdata.php';?>


<div id="whattolookforinmidfielderdiv">
    <div class="allsilversspacer">

        <div class="totwheaderbackground">    

            <div id="totwplayerheader">
                <div id="lefttotwheader">
                    <a href="alltips.php"><div id="alltotwbutton">ALL TIPS</div></a>
                </div>
                <div id="centertotwheader">
                    <div id="informplayerheader">what to look for in a silver midfielder</div>
                </div>
                <div id="righttotwheader">
                    <a href="whattolookforinawinger.php"><div id="nexttotwbutton">NEXT TIP</div></a>
                </div>
            </div>
        </div>

            <div class="totwheaderspacer"></div>

        

        <div class="tipsbackground">
            
            <div class="totwplayersrow1">
                
                    
                    
                        <div class="tipsheaderimage">
                            <img id="cbheaderimg" src="img/midfieldersgraphic.png">
                        </div>
                        <div class="tipstext">
                           Picking the right midfielders for your team usually depends on the formation. Midfielders are easy to choose because it’s obvious which players stand out and which don’t. The only times people tend to struggle with midfielders is when they start messing around with positions and use attacking midfielders as defensive midfielders and vice versa for chemistry reasons. The rule I usually stick to is that I won’t change the position of a midfielder more than once. For example, I’ll occasionally change a CAM to a CM or a CDM to a CM but never change them further. As I mentioned, good midfielders are more obvious than other positions so I’ll keep this shorter by explaining what I look for in each of the midfield positions based on the examples above.<br><br>

Sandro is an example of the perfect silver CDM. Although his base card is a CM, his ingame rating at CDM is 77 therefore he’s only a silver because of his position. This isn’t a bad thing though, it just means we have an incredible CDM. Sandro is my idea of a perfect CDM because he has everything. His defensive stats are incredible, he has medium/high workrates so he’ll sit back, he’s 6’1″ and his pace isn’t that bad either. Personally I always love using defensive midfielders with good shooting as well but defensive stats are what I look for first. Overall when picking a CDM focus on defensive stats and workrates over anything else. Pace is useful as a defensive midfielder but if he can’t win headers or control the midfield there’s much better options.<br><br>

As for the perfect CM, this usually depends on the formation. Because I never usually play 4-4-2 my central midfielders are often in one of the 4-3-3 variants meaning I tend to look for all round stats over everything. In this case Goretzka would be my ideal CM because he has solid stats all round with height as well as 4 star skills. It does depend on the formation though so I usually say that if I’m playing a flat 4-3-3 or 4-3-3 with a CAM I want my midfielders to be like Goretzka. Only if I’m using a variant like false-9 with a CDM will I then use slightly more attacking central midfielders.<br><br>

Lastly is the CAM, probably the easiest to pick out of the lot. N’Zogbia is my ideal with pace, dribbling, shooting and passing all above 70. Any CAM with all four of these stats above 70 will usually be a solid pick especially with 4 star skills. This is however the position where I do see people go wrong, opting for pace over shooting or dribbling. In my opinion, it’s better to have a CAM with 60 pace and great passing and shooting than a CAM with 90 pace and poor passing and shooting. Pace shouldn’t be the first thing you look for.
                        </div>
                    </div>
                
                   
            
           


        
    
        
    </div>
    


    </div>







<?php include 'footer.php';?>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script src="js/playerstars.js"></script>